﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace DB_Mid_Project
{
    public partial class CLO : Form
    {
    
        public CLO()
        {
            InitializeComponent();           
        }


        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
        //.....................view clo....................

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        //....................update clo......................
        private void update()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        //.................add in for clo................

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker1.CustomFormat = "yyyy/dd/mm";
            guna2DateTimePicker2.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker2.CustomFormat = "yyyy/dd/mm";
            guna2TextBox2.Text = guna2DateTimePicker1.Value.ToShortDateString();
            guna2TextBox3.Text = guna2DateTimePicker2.Value.ToShortDateString();
            if (guna2TextBox1.Text == "" || guna2TextBox2.Text == "" || guna2TextBox3.Text == "")
            {
                MessageBox.Show("Incomplete Info");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("if not exists(select 1 from clo where Name=@name) begin Insert into Clo (Name,DateCreated,DateUpdated) values (@name,@date,@date1) end ", con);
                cmd.Parameters.AddWithValue("@name", guna2TextBox1.Text);
                cmd.Parameters.AddWithValue("@date", guna2TextBox2.Text);
                cmd.Parameters.AddWithValue("@date1", guna2TextBox3.Text);
                int result=cmd.ExecuteNonQuery();
                if(result>0)
                {
                    MessageBox.Show("CLO Added Successfully");

                }
                else
                {
                    MessageBox.Show("CLO Already Exists..");
                }
                update();

            }
        }
        //.......................edit clo.................
        private void guna2Button3_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to Apply Changes to Clo", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Clo Set Name=@name,DateCreated=@date,DateUpdated=@date1  where Name=@name", con);
                cmd.Parameters.AddWithValue("@name", guna2TextBox1.Text);
                cmd.Parameters.AddWithValue("@date", guna2TextBox2.Text);
                cmd.Parameters.AddWithValue("@date1", guna2TextBox3.Text);

                cmd.ExecuteNonQuery();
                update();
            }
        }
       // ...................clo grid click...................
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            guna2TextBox1.Clear();
            guna2TextBox2.Clear();
            guna2TextBox3.Clear();

            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                guna2TextBox1.Text = selectedRow.Cells["Name"].Value.ToString();
                guna2TextBox2.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                guna2TextBox3.Text = selectedRow.Cells["DateUpdated"].Value.ToString();
                guna2TextBox1.ReadOnly = false;

            }
        }
        //..................save clo..............
        private void guna2Button16_Click(object sender, EventArgs e)
        {
            MessageBox.Show("All Changes Saved Successfully..");
        }
        //...................open student manage................
        private void guna2Button13_Click(object sender, EventArgs e)
        {
            this.Hide();
            Studentmanage s = new Studentmanage(guna2TabControl1);
            s.Show();
        }
        //.................open attendance manage.............
        private void guna2Button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Attendance_Form at = new Attendance_Form( guna2TabControl1);
            at.Show();
        }
        //.....................open clo tab.................
        private void guna2Button9_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 3;
        }

        //.............time  change for assessment compo..........
        private void guna2DateTimePicker1_ValueChanged_1(object sender, EventArgs e)
        {
            guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker1.CustomFormat = "yyyy/dd/mm";
        
            guna2TextBox2.Text = guna2DateTimePicker1.Value.ToShortDateString();

        }

        private void guna2DateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker2.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker2.CustomFormat = "yyyy/dd/mm";
            guna2TextBox3.Text = guna2DateTimePicker2.Value.ToShortDateString();
        }
        //...........................add all rubric to rubric id combobox ...........
        private void guna2Button10_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 1;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id from Rubric", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if(rubricIDBox.Items.Contains(reader["Id"].ToString())==false)
                {
                    rubricIDBox.Items.Add(reader["Id"].ToString());
                }
            }
            reader.Close();
        }
        //..............add name from clo in(clo id combobox) for rubric ..................
        private void guna2Button14_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Name from Clo", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if(guna2ComboBox1.Items.Contains(reader["Name"].ToString())==false)
                {
                    guna2ComboBox1.Items.Add(reader["Name"].ToString());
                }
        
            }
            reader.Close();

        }
        //............update rubric..........
        private void update_2()
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select r.id, r.details, r.cloId, c.Name from Rubric r join clo c on r.CLoid=c.id ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }
        //.................grid view for rubric.......
        private void guna2Button15_Click(object sender, EventArgs e)
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select r.id, r.details, r.cloId, c.Name from Rubric r join clo c on r.Cloid=c.id ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource=dt;
        }
        //..........add in rubric...........
        private void guna2Button17_Click(object sender, EventArgs e)
        {
            if(guna2TextBox5.Text==""||guna2ComboBox1.SelectedIndex==-1)
            {
                MessageBox.Show("Incomplete Information");
            }
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  if not exists(select details,cloid from rubric where Details=@detail and cloid=(select top 1 id from clo where Name=@name) ) begin insert into rubric (id,details,cloid) values ((select max(id)+1 from rubric),@detail,(select top 1 id from clo where Name=@name)) end", config);
            cmd.Parameters.AddWithValue("@detail", guna2TextBox5.Text);
            cmd.Parameters.AddWithValue("@name", guna2ComboBox1.SelectedItem.ToString());
            int res=cmd.ExecuteNonQuery();
            if(res==0)
            {
                MessageBox.Show("Rubric Already Exists..");
            }
            else
            {
                MessageBox.Show("rubric Added Successfully");
                update_2();
                
            }
        }
        //................update rubric................
        private void guna2Button18_Click(object sender, EventArgs e)
        {
            if (guna2TextBox5.Text == "" || guna2ComboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Incomplete Info");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Apply Changes to Rubric with id=" + label1.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" update rubric set details=@detail, CloId=(Select top 1 id from Clo where Name=@name) where id=@id ", config);
                    cmd.Parameters.AddWithValue("@detail", guna2TextBox5.Text);
                    cmd.Parameters.AddWithValue("@name", guna2ComboBox1.SelectedItem.ToString());
                    cmd.Parameters.AddWithValue("@id", int.Parse(label1.Text));

                    cmd.ExecuteNonQuery();
                    update_2();
                }
            }
        }

        //..................rubric grid click load.........
        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            guna2TextBox5.Clear();
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView2.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView2.Rows[e.RowIndex]; ;
                guna2ComboBox1.Text = selectedRow.Cells["Name"].Value.ToString();
                guna2TextBox5.Text = selectedRow.Cells["Details"].Value.ToString();
                label1.Text = selectedRow.Cells["Id"].Value.ToString();

            }
        }
//............add rubric level..............
        private void guna2Button22_Click(object sender, EventArgs e)
        {
            if (rubricIDBox.SelectedIndex == -1)
            {
                MessageBox.Show("Incomplete Information");
            }
            else
            {
                var config = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("if not exists (select top 1 RubricId from RubricLevel where RubricId=@rid) begin INSERT INTO RubricLevel (Details, RubricId, MeasurementLevel) SELECT DISTINCT Details, @Rid AS RubricId, MeasurementLevel FROM RubricLevel order by MeasurementLevel end", config);

                cmd.Parameters.AddWithValue("@rid", int.Parse(rubricIDBox.SelectedItem.ToString()));
                int res = cmd.ExecuteNonQuery();
                if (res < 1)
                {
                    MessageBox.Show("Rubric Level Already Exists..");
                }
                else
                {
                    MessageBox.Show("rubric Level Set Successfully");
                    update_3();

                }
            }
        }
        //........exit button for clo.........
        private void guna2Button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //...............view rubric level........
        private void guna2Button21_Click(object sender, EventArgs e)
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }
        //.............update rubric level.............
        private void update_3()
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView3.DataSource = dt;
        }

        //...............save rubric level...........

        private void guna2Button19_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Saved Successfully....");
        }
        //...........open assessment tab...........
        private void guna2Button8_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 2;
        }
        //..............view assessment.............
        private void guna2Button20_Click(object sender, EventArgs e)
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView4.DataSource = dt;
        }
        //..............add assessment ...................
        private void guna2Button23_Click(object sender, EventArgs e)
        {
            if (title.Text == "" || dateassesssment.Text == "" || tmarks.Text == "" || tweight.Text == "")
            {
                MessageBox.Show("Incomplete Information");
            }
            else
            {
                var config = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("if not exists (select top 1 title from Assessment where Title=@titl)  and not exists(select * from Assessment where (select sum(TotalWeightage)+@tw from Assessment)>100 )  begin INSERT INTO Assessment (Title, DateCreated, TotalMarks,TotalWeightage) Values(@titl,@date,@tm,@tw)end", config);

                cmd.Parameters.AddWithValue("@tw", int.Parse(tweight.Text));
                cmd.Parameters.AddWithValue("@tm", int.Parse(tmarks.Text));
                cmd.Parameters.AddWithValue("@date", dateassesssment.Text);
                cmd.Parameters.AddWithValue("@titl", title.Text);
                int res = cmd.ExecuteNonQuery();
                if (res < 1)
                {
                    MessageBox.Show("Assessment with this Title Already Exists or Weightage reacehed 100 or above...");
                }
                else
                {
                    MessageBox.Show("Assessment Added Successfully");
                    update_4();

                }
            }
        }
        //..................update assessment...........
        private void update_4()
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView4.DataSource = dt;
        }
        //.....................assessment date save in box.............
        private void guna2DateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker3.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker3.CustomFormat = "yyyy/dd/mm";
            dateassesssment.Text = guna2DateTimePicker3.Value.ToShortDateString();
        }
        //............load assessment from grid toedit...............
        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            tmarks.Clear();
            title.Clear();
            dateassesssment.Clear();
            tweight.Clear();
            id.Text = "None";
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView4.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView4.Rows[e.RowIndex]; ;
                title.Text = selectedRow.Cells["Title"].Value.ToString();
                dateassesssment.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                tmarks.Text = selectedRow.Cells["TotalMarks"].Value.ToString();
                tweight.Text = selectedRow.Cells["TotalWeightage"].Value.ToString();
                id.Text = selectedRow.Cells["Id"].Value.ToString();

            }
        }
        //.....................edit assessment............
        private void guna2Button2_Click(object sender, EventArgs e)
        {

            if (title.Text == "" || tmarks.Text == "" || tweight.Text == "" || dateassesssment.Text == "" || id.Text == "None")
            {
                MessageBox.Show("Incomplete Info");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Apply Changes to Assessment with id=" + id.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" update Assessment set Title=@titl,DateCreated=@date,TotalMarks=@tm,TotalWeightage=@tw Where  id=@id", config);
                    cmd.Parameters.AddWithValue("@tw", int.Parse(tweight.Text));
                    cmd.Parameters.AddWithValue("@tm", int.Parse(tmarks.Text));
                    cmd.Parameters.AddWithValue("@date", dateassesssment.Text);
                    cmd.Parameters.AddWithValue("@titl", title.Text);
                    cmd.Parameters.AddWithValue("@id", int.Parse(id.Text));
                    cmd.ExecuteNonQuery();
                    update_4();
                    id.Text = "None";
                }
                   
            }
        }
        //.................remove assessment..............
        private void guna2Button26_Click(object sender, EventArgs e)
        {
            if (id.Text == "None")
            {
                MessageBox.Show("please Select An id from table");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Remove Assessment with id=" + id.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(result== DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" delete from assessment where id=@id ", config);
                    cmd.Parameters.AddWithValue("@id", int.Parse(id.Text));
                    cmd.ExecuteNonQuery();
                    update_4();
                }
               
            }
        }
        //.............save in combo boxesfor asess compo........
        private void guna2Button24_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 4;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id from Rubric", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (RidcomboBox.Items.Contains(reader["Id"].ToString())==false)
                {
                    RidcomboBox.Items.Add(reader["Id"].ToString());
                }

            }
            reader.Close();
            SqlCommand cmd1 = new SqlCommand("select Id from Assessment", con);
            reader = cmd1.ExecuteReader();
            while(reader.Read())
            {

                if (assessmentidComboBox.Items.Contains(reader["Id"].ToString())==false)
                {
                    assessmentidComboBox.Items.Add(reader["Id"].ToString());
                }
            }
            reader.Close();
        }

        //......load data in grid for asess compo......... 
        private void guna2Button27_Click(object sender, EventArgs e)
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView5.DataSource = dt;
        }
        //..........update assessment component......
        private void update_5()
        {

            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent ", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView5.DataSource = dt;
        }
        //...........save date created and update for assessment compo in date boxes.............
        private void guna2DateTimePicker5_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker5.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker5.CustomFormat = "yyyy/dd/mm";
            DateCreated.Text = guna2DateTimePicker5.Value.ToShortDateString();
        }

        private void guna2DateTimePicker4_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker4.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker4.CustomFormat = "yyyy/dd/mm";
            dateUpdated.Text = guna2DateTimePicker4.Value.ToShortDateString();
        }
        //............add assessment component..............
        private void guna2Button30_Click(object sender, EventArgs e)
        {
            if (componentname.Text == "" || compoMarks.Text == "" || dateUpdated.Text == "" || DateCreated.Text == "" || RidcomboBox.SelectedIndex == -1 || assessmentidComboBox.SelectedIndex == -1 )
            {
                MessageBox.Show("Incomplete Information");
            }
            else
            {
                var config = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("if not exists (select top 1 id from assessment where totalMarks<(select sum(totalMArks)+@marks from assessmentcomponent where AssessmentID=@aid )) and not exists(select id from AssessmentComponent where name =@name and AssessmentId=@aid) begin insert into AssessmentComponent (Name,RubricId,DateCreated,TotalMarks, DateUpdated,  AssessmentId)values(@name,@rid,@datec,@marks,@dateu,@aid ) end", config);
                cmd.Parameters.AddWithValue("@marks", int.Parse(compoMarks.Text));
                cmd.Parameters.AddWithValue("@rid", int.Parse(RidcomboBox.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@datec", DateCreated.Text);
                cmd.Parameters.AddWithValue("@dateu", dateUpdated.Text);
                cmd.Parameters.AddWithValue("@name", componentname.Text);
                cmd.Parameters.AddWithValue("@aid", int.Parse(assessmentidComboBox.SelectedItem.ToString()));

                int res = cmd.ExecuteNonQuery();
                if (res < 1)
                {
                    MessageBox.Show("Assessment Component with this Name Already Exists or Cumulative Total Marks are OUT OF RANGE(Greater than Assessment Marks) ..");
                }
                else
                {
                    MessageBox.Show("Assessment Component Added Successfully ..");

                    update_5();
                }
            }
        }
        //.............update assessment component ................

        private void guna2Button28_Click(object sender, EventArgs e)
        {
            if (componentname.Text == "" || compoMarks.Text == "" || dateUpdated.Text == "" || DateCreated.Text == "" || RidcomboBox.SelectedIndex == -1 || assessmentidComboBox.SelectedIndex == -1||ACId.Text=="N")
            {
                MessageBox.Show("Incomplete Information");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Apply Changes to Assessment Component with id=" + ACId.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update AssessmentComponent set Name=@name,RubricId=@rid,DateCreated=@datec,TotalMarks=@marks, DateUpdated=@dateu ,  AssessmentId=@aid where Id=@id", config);
                    cmd.Parameters.AddWithValue("@marks", int.Parse(compoMarks.Text));
                    cmd.Parameters.AddWithValue("@rid", int.Parse(RidcomboBox.SelectedItem.ToString()));
                    cmd.Parameters.AddWithValue("@datec", DateCreated.Text);
                    cmd.Parameters.AddWithValue("@dateu", dateUpdated.Text);
                    cmd.Parameters.AddWithValue("@name", componentname.Text);
                    cmd.Parameters.AddWithValue("@aid", int.Parse(assessmentidComboBox.SelectedItem.ToString()));
                    cmd.Parameters.AddWithValue("@id", ACId.Text);

                    int res = cmd.ExecuteNonQuery();
                    if (res < 1)
                    {
                        MessageBox.Show("Assessment Component with this Name Already Exists or Cumulative Total Marks are OUT OF RANGE(Greater than Assessment Marks) ..");
                    }
                    else
                    {
                        
                        ACId.Text = "N";
                        update_5();
                    }
                }

            }
        }
        //........load assessment component from grid to boxes....................
        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ACId.Text = "N";
            componentname.Clear();
            compoMarks.Clear();
            DateCreated.Clear();
            dateUpdated.Clear();
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView5.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView5.Rows[e.RowIndex]; ;
                RidcomboBox.Text = selectedRow.Cells["RubricId"].Value.ToString();
                assessmentidComboBox.Text = selectedRow.Cells["AssessmentId"].Value.ToString();
                compoMarks.Text = selectedRow.Cells["TotalMarks"].Value.ToString();
                DateCreated.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                dateUpdated.Text = selectedRow.Cells["DateUpdated"].Value.ToString();
                componentname.Text = selectedRow.Cells["Name"].Value.ToString();
                ACId.Text = selectedRow.Cells["Id"].Value.ToString();
            }
        }
        //.........remove asessment component...............
        private void guna2Button29_Click(object sender, EventArgs e)
        {
            if (ACId.Text == "N")
            {
                MessageBox.Show("please Select An id from table");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Remove Assessment Component with id=" + ACId.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" delete from AssessmentComponent where id=@id ", config);
                    cmd.Parameters.AddWithValue("@id", int.Parse(ACId.Text));
                    cmd.ExecuteNonQuery();
                    update_5();
                }

            }
        }
        //........load in ids comboxes .........
        private void guna2Button11_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 5;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select l.Id AS Id,l.Details AS Level,r.Details As Name from RubricLevel l join rubric r on l.rubricid=r.id", con);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (RMidcombobox.Items.Contains(reader["Id"].ToString() + "," + reader["Level"].ToString() + "," + reader["Name"].ToString()) == false)
                {
                    RMidcombobox.Items.Add(reader["Id"].ToString()+","+ reader["Level"].ToString()+","+ reader["Name"].ToString());
                }

            }
            reader.Close();
            SqlCommand cmd1 = new SqlCommand("select Id,RegistrationNumber from Student where status=5", con);
            reader = cmd1.ExecuteReader();
            while (reader.Read())
            {
                if (sidcombobox.Items.Contains(reader["Id"].ToString() + "," + reader["RegistrationNumber"].ToString()) == false)
                {
                   sidcombobox.Items.Add(reader["Id"].ToString() + "," + reader["RegistrationNumber"].ToString());
                }

            }
            reader.Close();
            SqlCommand cmd2 = new SqlCommand("select c.Id AS Id,c.Name AS Name,a.Title AS Tital from AssessmentComponent c join Assessment a on c.AssessmentId=a.id", con);
            reader = cmd2.ExecuteReader();
            while (reader.Read())
            {
                if (acidcombobox.Items.Contains(reader["Id"].ToString() + "," + reader["Name"].ToString() + "," + reader["Tital"].ToString()) == false)
                {
                    acidcombobox.Items.Add(reader["Id"].ToString() + "," + reader["Name"].ToString()+"," + reader["Tital"].ToString());
                }

            }
            reader.Close();
        }
        //..............view result.........
        private void guna2Button32_Click(object sender, EventArgs e)
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select s.RegistrationNumber As Reg_No,a.Title AS Type,ac.Name As Component,ac.TotalMarks AS Component_Marks,sr.RubricMeasurementId as MeasurementId,rl.MeasurementLevel AS Student_RubricLevel,(cast((rl.MeasurementLevel/4.0 )*ac.TotalMarks as float )) As ObtainedMarks ,sr.EvaluationDate AS EvaluationDate from StudentResult sr join AssessmentComponent ac on sr.AssessmentComponentId=ac.Id join Assessment a on ac.AssessmentId=a.Id join RubricLevel rl on sr.RubricMeasurementId=rl.id join Rubric r on rl.RubricId=r.Id join Student s on sr.StudentId=s.Id where s.Status=5", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView6.DataSource = dt;
        }
        //...........upadate result............
        private void update_6()
        {
            var config = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select s.RegistrationNumber As Reg_No,a.Title AS Type,ac.Name As Component,ac.TotalMarks AS Component_Marks, sr.RubricMeasurementId as MeasurementId,rl.MeasurementLevel AS Student_RubricLevel,(cast((rl.MeasurementLevel/4.0 )*ac.TotalMarks as float )) As ObtainedMarks,sr.EvaluationDate AS EvaluationDate from StudentResult sr join AssessmentComponent ac on sr.AssessmentComponentId=ac.Id join Assessment a on ac.AssessmentId=a.Id join RubricLevel rl on sr.RubricMeasurementId=rl.id join Rubric r on rl.RubricId=r.Id join Student s on sr.StudentId=s.Id where s.Status=5", config);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView6.DataSource = dt;
        }
        //............add result.............
        private void guna2Button33_Click(object sender, EventArgs e)
        {
            try
            {
                if (dateEvaluation.Text == "" || sidcombobox.SelectedIndex == -1 || RMidcombobox.SelectedIndex == -1 || acidcombobox.SelectedIndex == -1)
                {
                    MessageBox.Show("Incomplete Information");
                }
                else
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("if not exists (select top 1 RubricMeasurementId,StudentId,AssessmentComponentId from StudentResult where RubricMeasurementId=@rmid and StudentId=@sid and AssessmentComponentId=@acid) begin INSERT INTO StudentResult (RubricMeasurementId,StudentId,AssessmentComponentId,EvaluationDate) Values(@rmid,@sid,@acid,@datee) end", config);

                    cmd.Parameters.AddWithValue("@rmid", int.Parse(RMidcombobox.SelectedItem.ToString().Split(',')[0]));
                    cmd.Parameters.AddWithValue("@sid", int.Parse(sidcombobox.SelectedItem.ToString().Split(',')[0]));
                    cmd.Parameters.AddWithValue("@datee", dateEvaluation.Text);
                    cmd.Parameters.AddWithValue("@acid", int.Parse(acidcombobox.SelectedItem.ToString().Split(',')[0]));
                    int res = cmd.ExecuteNonQuery();
                    if (res < 1)
                    {
                        MessageBox.Show("Student's this Assessment Component Already Evaluatd..");
                    }
                    else
                    {
                        MessageBox.Show("Student Evaluated Successfully");
                        update_6();

                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //.......remove result............
        private void guna2Button34_Click(object sender, EventArgs e)
        {
            if (resultId.Text == "N")
            {
                MessageBox.Show("please Select An id from table");
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to delete Result with registration Number=" + resultId.Text, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var config = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" delete from studentresult where studentId=(select id from student where registrationnumber=@reg) ", config);
                    cmd.Parameters.AddWithValue("@reg", int.Parse(resultId.Text));
                    cmd.ExecuteNonQuery();
                    update_6();
                }

            }
        }
        //.............load resultfrom grid to edit boxes.........
        private void dataGridView6_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            resultId.Text = "N";
            dateEvaluation.Clear();
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView6.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView6.Rows[e.RowIndex]; ;
                RMidcombobox.Text = selectedRow.Cells["MeasurementId"].Value.ToString();
                dateEvaluation.Text = selectedRow.Cells["EvaluationDate"].Value.ToString();
                resultId.Text = selectedRow.Cells["Reg_No"].Value.ToString();
            }
        }

        private void guna2DateTimePicker6_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker6.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker6.CustomFormat = "yyyy/dd/mm";
            dateEvaluation.Text = guna2DateTimePicker6.Value.ToShortDateString();
        }
        //..............edit result.................
        private void guna2Button31_Click(object sender, EventArgs e)
        {

        }
        //.......report query..........
        private void guna2Button37_Click(object sender, EventArgs e)
        {
            string assessmentresult = @"select s.RegistrationNumber As Reg_No,
a.Title AS Type,
a.TotalMarks AS total_Marks,
Sum((cast((rl.MeasurementLevel/4.0 )*ac.TotalMarks as float ))) As ObtainedMarks 
from StudentResult sr 
join AssessmentComponent ac on sr.AssessmentComponentId=ac.Id 
join Assessment a on ac.AssessmentId=a.Id 
join RubricLevel rl on sr.RubricMeasurementId=rl.id 
join Rubric r on rl.RubricId=r.Id 
join Student s on sr.StudentId=s.Id 
where s.Status=5 
group by s.RegistrationNumber,a.Title,a.TotalMarks
";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(assessmentresult, con);
            SqlDataAdapter d = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            d.Fill(data);
            GenerateReports.CreatePDF(data, "Assessment Result Report", "Assessment Wise Class Report");

        }
        //.............................report query2............
        private void report1button_Click(object sender, EventArgs e)
        {
             string resultqiery = @"select Student.FirstName + ' ' + Student.LastName as [student name], 
Student.RegistrationNumber as [registration number], Clo.Name as [clo name],
    sum(AssessmentComponent.TotalMarks) as [total marks],
    sum((cast(RubricLevel.MeasurementLevel as float) / [max measurement level].maxLevel) * AssessmentComponent.TotalMarks) as [obtained marks],
    ((sum((cast(RubricLevel.MeasurementLevel as float) / [max measurement level].maxLevel) * AssessmentComponent.TotalMarks) * 100) /
    sum(AssessmentComponent.TotalMarks)) as [percentage]
from Student
join StudentResult on Student.Id = StudentResult.StudentId
join RubricLevel on StudentResult.RubricMeasurementId = RubricLevel.Id
join AssessmentComponent on AssessmentComponent.Id = StudentResult.AssessmentComponentId
join Assessment on Assessment.Id = AssessmentComponent.AssessmentId
join Rubric on Rubric.Id = RubricLevel.RubricId
join Clo on Clo.Id = Rubric.CloId
join ( select RubricLevel.RubricId as rid,  max(RubricLevel.MeasurementLevel) as maxLevel from RubricLevel
 group by RubricLevel.RubricId ) as [max measurement level] on [max measurement level].rid = Rubric.Id
group by Clo.Name, Student.FirstName, Student.LastName, Student.RegistrationNumber";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(resultqiery, con);
            SqlDataAdapter d = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            d.Fill(data);
            GenerateReports.CreatePDF(data, "Result Report", "CLO Wise Class Report");


        }
        //................report by provided query...........
        private void generateButton_Click(object sender, EventArgs e)
        { 
            try
            {
                var con = Configuration.getInstance().getConnection();
                string query = querybox.Text;
                if(query.Split(' ')[0]!="Select"|| query.Split(' ')[0] != "select")
                {
                    MessageBox.Show("You cannot Change or Delete data from database.. ");
                }
                else
                {
                    string fileName = titlebox.Text;
                    SqlCommand cmd = new SqlCommand(query, con);
                    SqlDataAdapter d = new SqlDataAdapter(cmd);
                    DataTable data = new DataTable();
                    d.Fill(data);
                    GenerateReports.CreatePDF(data, fileName, tablename.Text);
                    string pdfFilePath = $@"C:\Users\MUHAMMAD AWAIS\OneDrive\Desktop\DB LAB\mid project\DB_Mid_Project\bin\Debug\{fileName}.pdf.pdf";
                    OpenPdfInChrome(pdfFilePath);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        //...........report inactive student.........
        private void report5buttton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student where status=6", con);
            SqlDataAdapter d = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            d.Fill(data);
            GenerateReports.CreatePDF(data, "InActive Students", "Table 5");
        }
        //.......open report tab..............
        private void guna2Button25_Click(object sender, EventArgs e)
        {
            guna2TabControl1.SelectedIndex = 6;
        }
        //...................report query4....................
        private void report3button_Click(object sender, EventArgs e)
        {
            string attendance = @"select Student.FirstName, Student.LastName,
    count(*) as [Total Marked],
    sum(case StudentAttendance.AttendanceStatus when '1' then 1 else 0 end) as Present,
    sum(case StudentAttendance.AttendanceStatus when '2' then 1 else 0 end) as Absent,
    sum(case StudentAttendance.AttendanceStatus when '3' then 1 else 0 end) as Leave,
    sum(case StudentAttendance.AttendanceStatus when '4' then 1 else 0 end) as Late,
    cast((cast(sum(case StudentAttendance.AttendanceStatus when '1' then 1 else 0 end) as float) / count(*) * 100) as varchar(max)) + '%' as [Attendance Percentage]
from ClassAttendance
join StudentAttendance on ClassAttendance.Id = StudentAttendance.AttendanceId
join Student on Student.Id = StudentAttendance.StudentId
join Lookup on Lookup.LookupId = StudentAttendance.AttendanceStatus
group by Student.FirstName, Student.LastName";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(attendance, con);
            SqlDataAdapter d = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            d.Fill(data);
            GenerateReports.CreatePDF(data, "Attendance Report", "Attendance Class Report");
        }
        //.........report query 5.........
        private void report4button_Click(object sender, EventArgs e)
        {
            string res = @"select s.RegistrationNumber As Reg_No,
a.Title AS Type,ac.Name As Component,
ac.TotalMarks AS Component_Marks,sr.RubricMeasurementId as MeasurementId,
rl.MeasurementLevel AS Student_RubricLevel,
(cast((rl.MeasurementLevel/4.0 )*ac.TotalMarks as float )) As ObtainedMarks ,
sr.EvaluationDate AS EvaluationDate 
from StudentResult sr 
join AssessmentComponent ac on sr.AssessmentComponentId=ac.Id 
join Assessment a on ac.AssessmentId=a.Id 
join RubricLevel rl on sr.RubricMeasurementId=rl.id 
join Rubric r on rl.RubricId=r.Id 
join Student s on sr.StudentId=s.Id 
where s.Status=5";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(res, con);
            SqlDataAdapter d = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            d.Fill(data);
            GenerateReports.CreatePDF(data, "Result Report", "Result Report with Evaluation DATE");
        }
        
       // .........open pdf in chrome...........
        static void OpenPdfInChrome(string pdfFilePath)
        {
            try
            {
                Process.Start("chrome.exe", $"\"{pdfFilePath}\"");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
    }
}

