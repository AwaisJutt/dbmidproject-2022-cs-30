﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace DB_Mid_Project
{
    internal class GenerateReports
    {
        public static void CreatePDF(DataTable dt, string fileName, string title)
        {
            try
            {
                fileName = $"C:\\Users\\MUHAMMAD AWAIS\\OneDrive\\Desktop\\DB LAB\\mid project\\DB_Mid_Project\\bin\\Debug\\{fileName}.pdf";
                string basePath = AppDomain.CurrentDomain.BaseDirectory;
                string relativePath = Path.Combine("PDF Reports", $"{fileName}.pdf");
                fileName = Path.Combine(basePath, relativePath);

                Document document = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));

                // Header and Footer
                HeaderFooter eventHandler = new HeaderFooter();
                writer.PageEvent = eventHandler;

                document.Open();

                // Title
                AddTitle(document, title);

                // Table
                AddDataTable(document, dt);

                document.Close();

                MessageBox.Show("PDF created successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error in creating PDF: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void AddTitle(Document document, string title)
        {
            // Title Font
            BaseFont titleBaseFont = BaseFont.CreateFont("C:\\Users\\MUHAMMAD AWAIS\\OneDrive\\Desktop\\DB LAB\\mid project\\DB_Mid_Project\\mvboli.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font titleFont = new Font(titleBaseFont, 16, Font.BOLD, BaseColor.BLACK);

            // Title Paragraph
            Paragraph titleParagraph = new Paragraph(title, titleFont);
            titleParagraph.Alignment = Element.ALIGN_CENTER;
            titleParagraph.SpacingAfter = 10f;

            document.Add(titleParagraph);
        }

        private static void AddDataTable(Document document, DataTable dt)
        {
            BaseFont tableBaseFont = BaseFont.CreateFont("C:\\Users\\MUHAMMAD AWAIS\\OneDrive\\Desktop\\DB LAB\\mid project\\DB_Mid_Project\\TEMPSITC.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font tableFont = new Font(tableBaseFont, 12, Font.NORMAL, BaseColor.BLACK);
            BaseColor evenRowColor = new BaseColor(169, 169, 169); 
            BaseColor oddRowColor = new BaseColor(192, 192, 192); 
            PdfPTable table = new PdfPTable(dt.Columns.Count);
            float[] widths = new float[dt.Columns.Count];
            for (int i = 0; i < dt.Columns.Count; i++)
                widths[i] = 4f;

            table.SetWidths(widths);
            table.WidthPercentage = 100;
            foreach (DataColumn c in dt.Columns)
            {
                PdfPCell headerCell = new PdfPCell(new Phrase(c.ColumnName, tableFont));
                headerCell.BackgroundColor = evenRowColor;
                table.AddCell(headerCell);
            }

            for (int rowIndex = 0; rowIndex < dt.Rows.Count; rowIndex++)
            {
                for (int colIndex = 0; colIndex < dt.Columns.Count; colIndex++)
                {
                    PdfPCell dataCell = new PdfPCell(new Phrase(dt.Rows[rowIndex][colIndex].ToString(), tableFont));
                    dataCell.BackgroundColor = (rowIndex % 2 == 0) ? evenRowColor : oddRowColor; 
                    table.AddCell(dataCell);
                }
            }

            document.Add(table);
        }
    }

    internal class HeaderFooter : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable headerTable = new PdfPTable(1);
            headerTable.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
            headerTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            headerTable.AddCell(new Phrase("Created by: Muhammad Awais", FontFactory.GetFont("Impact", 14, Font.BOLDITALIC, BaseColor.BLACK)));
            PdfPTable footerTable = new PdfPTable(1);
            footerTable.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
            footerTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTable.AddCell(new Phrase($"Created on: {DateTime.Now:yyyy-MM-dd HH:mm:ss}", FontFactory.GetFont("Lato", 16, Font.BOLDITALIC, BaseColor.BLACK)));
            headerTable.WriteSelectedRows(0, -1, document.LeftMargin, document.PageSize.Height - document.TopMargin + headerTable.TotalHeight, writer.DirectContent);
            footerTable.WriteSelectedRows(0, -1, document.LeftMargin, document.BottomMargin - footerTable.TotalHeight, writer.DirectContent);
        }
    }
}
