﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace DB_Mid_Project
{
    public partial class Studentmanage : Form
    {
        private Guna.UI2.WinForms.Guna2TabControl cloTabControl;
        private CLO cloform;
        public Studentmanage(Guna.UI2.WinForms.Guna2TabControl tabControl)
        {
            InitializeComponent();
            panel2.Hide();

            guna2CustomGradientPanel1.Hide();
            cloform = new CLO();
            comboBox1.SelectedIndex= 0;
            cloTabControl = tabControl;
        }


        
        private void update_table()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.FirstName,s.LastName,s.Contact,s.Email,s.RegistrationNumber,l.Name As CurrentStatus from Student s join Lookup l on s.Status=l.LookupId where s.Status=5 ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {
                
                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                guna2TextBox2.Text = selectedRow.Cells["LastName"].Value.ToString();
                guna2TextBox1.Text = selectedRow.Cells["FirstName"].Value.ToString();
                guna2TextBox3.Text = selectedRow.Cells["Contact"].Value.ToString();
                guna2TextBox4.Text = selectedRow.Cells["Email"].Value.ToString();
                guna2TextBox5.Text = selectedRow.Cells["RegistrationNumber"].Value.ToString();



            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (guna2TextBox2.Text == "" || guna2TextBox3.Text == "" || guna2TextBox4.Text == "" || guna2TextBox5.Text == ""|| guna2TextBox1.Text=="" )
            {
                MessageBox.Show("incomplete or Incorrect information");
            }
            if (guna2TextBox3.Text.Length != 11 || !guna2TextBox3.Text.All(char.IsDigit))
            {
                MessageBox.Show("Please enter a valid 11-digit contact number.");
                return;
            }
            string emailPattern = @"^\S+@gmail\.com";
            if (!System.Text.RegularExpressions.Regex.IsMatch(guna2TextBox4.Text, emailPattern))
            {
                MessageBox.Show("Please enter a valid Gmail address @gmail.com .");
                return;
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Student values ( @FirstName, @LastName, @Contact, @Email,@RegistrationNumber,@Status)", con);
                cmd.Parameters.AddWithValue("@FirstName", guna2TextBox1.Text);
                cmd.Parameters.AddWithValue("@LastName", guna2TextBox2.Text);
                cmd.Parameters.AddWithValue("@Contact", guna2TextBox3.Text);
                cmd.Parameters.AddWithValue("@Email", guna2TextBox4.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", guna2TextBox5.Text);
                cmd.Parameters.AddWithValue("@Status", 5);

                cmd.ExecuteNonQuery();
                guna2TextBox3.Clear();
                guna2TextBox2.Clear();
                guna2TextBox4.Clear();
                guna2TextBox5.Clear();
                guna2TextBox1.Clear();

                MessageBox.Show("Successfully Added");

                update_table();
            }
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.FirstName,s.LastName,s.Contact,s.Email,s.RegistrationNumber,l.Name As CurrentStatus from Student s join Lookup l on s.Status=l.LookupId where s.Status=5 ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            if (guna2TextBox2.Text == "" || guna2TextBox3.Text == "" || guna2TextBox4.Text == "" || guna2TextBox5.Text == "" || guna2TextBox1.Text == "" )
            {
                MessageBox.Show("incomplete or Incorrect information");
            }
            string updateQuery = "UPDATE Student SET FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, RegistrationNumber = @RegistrationNumber, Status = @Status WHERE RegistrationNumber = @RegistrationNumber";


            SqlCommand cmd = new SqlCommand(updateQuery, con);
            cmd.Parameters.AddWithValue("@LastName", guna2TextBox2.Text);
            cmd.Parameters.AddWithValue("@FirstName", guna2TextBox1.Text);
            cmd.Parameters.AddWithValue("@Contact", guna2TextBox3.Text);
            cmd.Parameters.AddWithValue("@Email", guna2TextBox4.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", guna2TextBox5.Text);
            cmd.Parameters.AddWithValue("@Status", 5);
            cmd.ExecuteNonQuery();
            guna2TextBox3.Clear();
            guna2TextBox2.Clear();
            guna2TextBox4.Clear();
            guna2TextBox5.Clear();
            guna2TextBox1.Clear();
            MessageBox.Show("Successfully updated information");
            update_table();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            guna2CustomGradientPanel1.Show();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Student set Status=6 WHERE RegistrationNumber = @RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", guna2TextBox7.Text);
            int exist = cmd.ExecuteNonQuery();
            if (exist > 0)
            {
                MessageBox.Show("Student RegistrationNumber " + guna2TextBox7.Text + " removed successfully.");
            }
            else
            {
                MessageBox.Show("Student Having RegistrationNumber " + guna2TextBox7.Text + " does not exist.");
                guna2TextBox7.Clear();
            }
        }
        private void loadfrom_csv()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student (FirstName, LastName, Contact, Email, RegistrationNumber, [Status]) values (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber, @Status)", con);

          
            string[] csvLines = File.ReadAllLines("data.csv");

            foreach (var line in csvLines.Skip(0))
            {
                string[] values = line.Split(',');
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@FirstName", values[3]);
                cmd.Parameters.AddWithValue("@LastName", values[4]);
                cmd.Parameters.AddWithValue("@Contact", values[5]);
                cmd.Parameters.AddWithValue("@Email", values[6]);
                cmd.Parameters.AddWithValue("@RegistrationNumber", values[2]);
                cmd.Parameters.AddWithValue("@Status", 5);

                cmd.ExecuteNonQuery();
            }
        }
        private void guna2Button6_Click(object sender, EventArgs e)
        {
            guna2CustomGradientPanel1.Hide();
            update_table();

        }

        private void guna2Button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Button13_Click(object sender, EventArgs e)
        {
            this.Hide();
            Attendance_Form atf = new Attendance_Form(cloTabControl);
            atf.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button16_Click(object sender, EventArgs e)
        {
          panel2.Show();
        }

        private void guna2Button14_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.FirstName,s.LastName,s.Contact,s.Email,s.RegistrationNumber,l.Name As CurrentStatus from Student s join Lookup l on s.Status=l.LookupId where s.Status=5 and RegistrationNumber=@RegistrationNumber ", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", guna2TextBox6.Text);
            int exist = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            if (exist == 0)
            {
                MessageBox.Show("Student Having RegistrationNumber " + guna2TextBox6.Text + " does not exist.");
                guna2TextBox7.Clear();
            }
        }

        private void guna2Button15_Click(object sender, EventArgs e)
        {
            panel2.Hide();
            update_table();
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 3;
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 2;

        }

        private void guna2Button18_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 4;

        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 0;
        }

        private void guna2Button10_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 1;
        }

        private void guna2Button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 5;


        }

        private void guna2Button17_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 6;
            CLO clo = new CLO();
        }
    }
}
