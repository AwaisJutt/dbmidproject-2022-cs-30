﻿
namespace DB_Mid_Project
{
    partial class CLO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.guna2TabControl1 = new Guna.UI2.WinForms.Guna2TabControl();
            this.RubricTab = new System.Windows.Forms.TabPage();
            this.guna2GradientPanel1 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button18 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button15 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button17 = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.guna2HtmlLabel3 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2ComboBox1 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2TextBox5 = new Guna.UI2.WinForms.Guna2TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.guna2HtmlLabel2 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel5 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel8 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.RubricLevelTab = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.guna2GradientPanel3 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button21 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button22 = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rubricIDBox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2Button19 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2HtmlLabel9 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel10 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.AssessmentTab = new System.Windows.Forms.TabPage();
            this.guna2GradientPanel4 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button20 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button26 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button23 = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.id = new System.Windows.Forms.Label();
            this.guna2HtmlLabel16 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2Button6 = new Guna.UI2.WinForms.Guna2Button();
            this.tweight = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2HtmlLabel15 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.tmarks = new Guna.UI2.WinForms.Guna2TextBox();
            this.dateassesssment = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2DateTimePicker3 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.title = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2HtmlLabel11 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel12 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel13 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel14 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.CloTab = new System.Windows.Forms.TabPage();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.guna2DateTimePicker2 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2DateTimePicker1 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2TextBox3 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.guna2HtmlLabel7 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel6 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel4 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2Button16 = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.AssessmentComponentTab = new System.Windows.Forms.TabPage();
            this.guna2GradientPanel5 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button27 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button28 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button29 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button30 = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ACId = new System.Windows.Forms.Label();
            this.compoMarks = new Guna.UI2.WinForms.Guna2TextBox();
            this.assessmentidComboBox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel24 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.DateCreated = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2DateTimePicker5 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2HtmlLabel22 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel21 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.dateUpdated = new Guna.UI2.WinForms.Guna2TextBox();
            this.RidcomboBox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2DateTimePicker4 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.componentname = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2HtmlLabel17 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel18 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel19 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel20 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.ResultTab = new System.Windows.Forms.TabPage();
            this.guna2GradientPanel6 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button31 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button34 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button32 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button33 = new Guna.UI2.WinForms.Guna2Button();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.resultId = new System.Windows.Forms.Label();
            this.acidcombobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.RMidcombobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel28 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.dateEvaluation = new Guna.UI2.WinForms.Guna2TextBox();
            this.sidcombobox = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2DateTimePicker6 = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2HtmlLabel23 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel25 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel26 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel27 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.ReportTab = new System.Windows.Forms.TabPage();
            this.generateButton = new Guna.UI2.WinForms.Guna2Button();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.guna2HtmlLabel32 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.tablename = new Guna.UI2.WinForms.Guna2TextBox();
            this.titlebox = new Guna.UI2.WinForms.Guna2TextBox();
            this.querybox = new System.Windows.Forms.RichTextBox();
            this.guna2HtmlLabel31 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel30 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel29 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2GradientPanel7 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.report5buttton = new Guna.UI2.WinForms.Guna2Button();
            this.report4button = new Guna.UI2.WinForms.Guna2Button();
            this.report3button = new Guna.UI2.WinForms.Guna2Button();
            this.report2button = new Guna.UI2.WinForms.Guna2Button();
            this.report1button = new Guna.UI2.WinForms.Guna2Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.guna2GradientPanel2 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2Button25 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button24 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button14 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button13 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2TextBox8 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2Button12 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button11 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button10 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button9 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button8 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button7 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2TabControl1.SuspendLayout();
            this.RubricTab.SuspendLayout();
            this.guna2GradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.RubricLevelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.guna2GradientPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.AssessmentTab.SuspendLayout();
            this.guna2GradientPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.CloTab.SuspendLayout();
            this.guna2CustomGradientPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.AssessmentComponentTab.SuspendLayout();
            this.guna2GradientPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.ResultTab.SuspendLayout();
            this.guna2GradientPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel6.SuspendLayout();
            this.ReportTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.guna2GradientPanel7.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.guna2GradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2TabControl1
            // 
            this.guna2TabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.guna2TabControl1.Controls.Add(this.RubricTab);
            this.guna2TabControl1.Controls.Add(this.RubricLevelTab);
            this.guna2TabControl1.Controls.Add(this.AssessmentTab);
            this.guna2TabControl1.Controls.Add(this.CloTab);
            this.guna2TabControl1.Controls.Add(this.AssessmentComponentTab);
            this.guna2TabControl1.Controls.Add(this.ResultTab);
            this.guna2TabControl1.Controls.Add(this.ReportTab);
            this.guna2TabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2TabControl1.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2TabControl1.ItemSize = new System.Drawing.Size(266, 44);
            this.guna2TabControl1.Location = new System.Drawing.Point(0, 0);
            this.guna2TabControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2TabControl1.Name = "guna2TabControl1";
            this.guna2TabControl1.SelectedIndex = 0;
            this.guna2TabControl1.Size = new System.Drawing.Size(901, 620);
            this.guna2TabControl1.TabButtonHoverState.BorderColor = System.Drawing.Color.Empty;
            this.guna2TabControl1.TabButtonHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(52)))), ((int)(((byte)(70)))));
            this.guna2TabControl1.TabButtonHoverState.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.guna2TabControl1.TabButtonHoverState.ForeColor = System.Drawing.Color.White;
            this.guna2TabControl1.TabButtonHoverState.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(52)))), ((int)(((byte)(70)))));
            this.guna2TabControl1.TabButtonIdleState.BorderColor = System.Drawing.Color.Empty;
            this.guna2TabControl1.TabButtonIdleState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(42)))), ((int)(((byte)(57)))));
            this.guna2TabControl1.TabButtonIdleState.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.guna2TabControl1.TabButtonIdleState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(160)))), ((int)(((byte)(167)))));
            this.guna2TabControl1.TabButtonIdleState.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(42)))), ((int)(((byte)(57)))));
            this.guna2TabControl1.TabButtonSelectedState.BorderColor = System.Drawing.Color.Empty;
            this.guna2TabControl1.TabButtonSelectedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(37)))), ((int)(((byte)(49)))));
            this.guna2TabControl1.TabButtonSelectedState.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.guna2TabControl1.TabButtonSelectedState.ForeColor = System.Drawing.Color.White;
            this.guna2TabControl1.TabButtonSelectedState.InnerColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(132)))), ((int)(((byte)(255)))));
            this.guna2TabControl1.TabButtonSize = new System.Drawing.Size(266, 44);
            this.guna2TabControl1.TabIndex = 0;
            this.guna2TabControl1.TabMenuBackColor = System.Drawing.Color.DarkCyan;
            this.guna2TabControl1.TabMenuVisible = false;
            // 
            // RubricTab
            // 
            this.RubricTab.BackColor = System.Drawing.SystemColors.ControlDark;
            this.RubricTab.Controls.Add(this.guna2GradientPanel1);
            this.RubricTab.Controls.Add(this.dataGridView2);
            this.RubricTab.Controls.Add(this.tableLayoutPanel2);
            this.RubricTab.Location = new System.Drawing.Point(5, 4);
            this.RubricTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RubricTab.Name = "RubricTab";
            this.RubricTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RubricTab.Size = new System.Drawing.Size(892, 612);
            this.RubricTab.TabIndex = 7;
            this.RubricTab.Text = "tabPage2";
            // 
            // guna2GradientPanel1
            // 
            this.guna2GradientPanel1.BackColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel1.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel1.BorderRadius = 2;
            this.guna2GradientPanel1.BorderThickness = 3;
            this.guna2GradientPanel1.Controls.Add(this.guna2Button18);
            this.guna2GradientPanel1.Controls.Add(this.guna2Button15);
            this.guna2GradientPanel1.Controls.Add(this.guna2Button17);
            this.guna2GradientPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2GradientPanel1.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel1.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel1.Location = new System.Drawing.Point(259, 530);
            this.guna2GradientPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel1.Name = "guna2GradientPanel1";
            this.guna2GradientPanel1.Size = new System.Drawing.Size(631, 79);
            this.guna2GradientPanel1.TabIndex = 4;
            // 
            // guna2Button18
            // 
            this.guna2Button18.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button18.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button18.BorderRadius = 6;
            this.guna2Button18.BorderThickness = 2;
            this.guna2Button18.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button18.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button18.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button18.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button18.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button18.ForeColor = System.Drawing.Color.White;
            this.guna2Button18.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button18.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button18.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button18.Image = global::DB_Mid_Project.Properties.Resources._24i_removebg_preview;
            this.guna2Button18.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button18.Location = new System.Drawing.Point(433, 16);
            this.guna2Button18.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button18.Name = "guna2Button18";
            this.guna2Button18.Size = new System.Drawing.Size(182, 43);
            this.guna2Button18.TabIndex = 5;
            this.guna2Button18.Text = "Edit Rubric";
            this.guna2Button18.Click += new System.EventHandler(this.guna2Button18_Click);
            // 
            // guna2Button15
            // 
            this.guna2Button15.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button15.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button15.BorderRadius = 6;
            this.guna2Button15.BorderThickness = 2;
            this.guna2Button15.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button15.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button15.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button15.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button15.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button15.ForeColor = System.Drawing.Color.White;
            this.guna2Button15.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button15.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button15.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button15.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button15.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button15.Location = new System.Drawing.Point(28, 16);
            this.guna2Button15.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button15.Name = "guna2Button15";
            this.guna2Button15.Size = new System.Drawing.Size(154, 43);
            this.guna2Button15.TabIndex = 2;
            this.guna2Button15.Text = "View Rubrics";
            this.guna2Button15.Click += new System.EventHandler(this.guna2Button15_Click);
            // 
            // guna2Button17
            // 
            this.guna2Button17.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button17.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button17.BorderRadius = 6;
            this.guna2Button17.BorderThickness = 2;
            this.guna2Button17.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button17.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button17.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button17.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button17.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button17.ForeColor = System.Drawing.Color.White;
            this.guna2Button17.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button17.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button17.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button17.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button17.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button17.Location = new System.Drawing.Point(228, 16);
            this.guna2Button17.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button17.Name = "guna2Button17";
            this.guna2Button17.Size = new System.Drawing.Size(166, 43);
            this.guna2Button17.TabIndex = 0;
            this.guna2Button17.Text = "Add Rubric";
            this.guna2Button17.Click += new System.EventHandler(this.guna2Button17_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(259, 3);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(631, 606);
            this.dataGridView2.TabIndex = 3;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.pictureBox4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.01245F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.98755F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox4.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox4.Location = new System.Drawing.Point(2, 3);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(251, 95);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkCyan;
            this.panel2.Controls.Add(this.guna2HtmlLabel3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.guna2ComboBox1);
            this.panel2.Controls.Add(this.guna2TextBox5);
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Controls.Add(this.guna2HtmlLabel2);
            this.panel2.Controls.Add(this.guna2HtmlLabel5);
            this.panel2.Controls.Add(this.guna2HtmlLabel8);
            this.panel2.Controls.Add(this.guna2Button5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(2, 106);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(251, 497);
            this.panel2.TabIndex = 1;
            // 
            // guna2HtmlLabel3
            // 
            this.guna2HtmlLabel3.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel3.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel3.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel3.Location = new System.Drawing.Point(8, 231);
            this.guna2HtmlLabel3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel3.Name = "guna2HtmlLabel3";
            this.guna2HtmlLabel3.Size = new System.Drawing.Size(70, 23);
            this.guna2HtmlLabel3.TabIndex = 28;
            this.guna2HtmlLabel3.Text = "Rubric Id:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 233);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "None";
            // 
            // guna2ComboBox1
            // 
            this.guna2ComboBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox1.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox1.ItemHeight = 30;
            this.guna2ComboBox1.Location = new System.Drawing.Point(38, 100);
            this.guna2ComboBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2ComboBox1.Name = "guna2ComboBox1";
            this.guna2ComboBox1.Size = new System.Drawing.Size(162, 36);
            this.guna2ComboBox1.TabIndex = 26;
            // 
            // guna2TextBox5
            // 
            this.guna2TextBox5.Animated = true;
            this.guna2TextBox5.BorderColor = System.Drawing.SystemColors.GrayText;
            this.guna2TextBox5.BorderRadius = 10;
            this.guna2TextBox5.BorderThickness = 2;
            this.guna2TextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox5.DefaultText = "";
            this.guna2TextBox5.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox5.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Location = new System.Drawing.Point(25, 290);
            this.guna2TextBox5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2TextBox5.Name = "guna2TextBox5";
            this.guna2TextBox5.PasswordChar = '\0';
            this.guna2TextBox5.PlaceholderText = "XYZ";
            this.guna2TextBox5.SelectedText = "";
            this.guna2TextBox5.Size = new System.Drawing.Size(200, 30);
            this.guna2TextBox5.TabIndex = 9;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox5.Image = global::DB_Mid_Project.Properties.Resources.k9i_removebg_preview;
            this.pictureBox5.Location = new System.Drawing.Point(2, 9);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(46, 45);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 7;
            this.pictureBox5.TabStop = false;
            // 
            // guna2HtmlLabel2
            // 
            this.guna2HtmlLabel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel2.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel2.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel2.Location = new System.Drawing.Point(55, 16);
            this.guna2HtmlLabel2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel2.Name = "guna2HtmlLabel2";
            this.guna2HtmlLabel2.Size = new System.Drawing.Size(167, 32);
            this.guna2HtmlLabel2.TabIndex = 6;
            this.guna2HtmlLabel2.Text = "Manage Rubrics";
            // 
            // guna2HtmlLabel5
            // 
            this.guna2HtmlLabel5.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel5.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel5.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel5.Location = new System.Drawing.Point(8, 260);
            this.guna2HtmlLabel5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel5.Name = "guna2HtmlLabel5";
            this.guna2HtmlLabel5.Size = new System.Drawing.Size(108, 23);
            this.guna2HtmlLabel5.TabIndex = 3;
            this.guna2HtmlLabel5.Text = "Rubric Details:";
            // 
            // guna2HtmlLabel8
            // 
            this.guna2HtmlLabel8.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel8.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel8.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel8.Location = new System.Drawing.Point(8, 71);
            this.guna2HtmlLabel8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel8.Name = "guna2HtmlLabel8";
            this.guna2HtmlLabel8.Size = new System.Drawing.Size(91, 23);
            this.guna2HtmlLabel8.TabIndex = 0;
            this.guna2HtmlLabel8.Text = "CLO Name:";
            // 
            // guna2Button5
            // 
            this.guna2Button5.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.guna2Button5.BorderRadius = 6;
            this.guna2Button5.BorderThickness = 2;
            this.guna2Button5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button5.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button5.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button5.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button5.Image = global::DB_Mid_Project.Properties.Resources._26_removebg_preview;
            this.guna2Button5.Location = new System.Drawing.Point(55, 433);
            this.guna2Button5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.Size = new System.Drawing.Size(103, 43);
            this.guna2Button5.TabIndex = 25;
            this.guna2Button5.Text = "Save";
            // 
            // RubricLevelTab
            // 
            this.RubricLevelTab.BackColor = System.Drawing.SystemColors.ControlDark;
            this.RubricLevelTab.Controls.Add(this.dataGridView3);
            this.RubricLevelTab.Controls.Add(this.guna2GradientPanel3);
            this.RubricLevelTab.Controls.Add(this.tableLayoutPanel4);
            this.RubricLevelTab.Location = new System.Drawing.Point(5, 4);
            this.RubricLevelTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RubricLevelTab.Name = "RubricLevelTab";
            this.RubricLevelTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RubricLevelTab.Size = new System.Drawing.Size(892, 612);
            this.RubricLevelTab.TabIndex = 8;
            this.RubricLevelTab.Text = "tabPage3";
            // 
            // dataGridView3
            // 
            this.dataGridView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(262, 0);
            this.dataGridView3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(696, 508);
            this.dataGridView3.TabIndex = 6;
            // 
            // guna2GradientPanel3
            // 
            this.guna2GradientPanel3.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel3.BorderThickness = 4;
            this.guna2GradientPanel3.Controls.Add(this.guna2Button21);
            this.guna2GradientPanel3.Controls.Add(this.guna2Button22);
            this.guna2GradientPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2GradientPanel3.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel3.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel3.Location = new System.Drawing.Point(259, 509);
            this.guna2GradientPanel3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel3.Name = "guna2GradientPanel3";
            this.guna2GradientPanel3.Size = new System.Drawing.Size(631, 100);
            this.guna2GradientPanel3.TabIndex = 5;
            // 
            // guna2Button21
            // 
            this.guna2Button21.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button21.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button21.BorderRadius = 6;
            this.guna2Button21.BorderThickness = 2;
            this.guna2Button21.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button21.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button21.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button21.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button21.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button21.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button21.ForeColor = System.Drawing.Color.White;
            this.guna2Button21.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button21.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button21.HoverState.Font = new System.Drawing.Font("MV Boli", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button21.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button21.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button21.Location = new System.Drawing.Point(43, 27);
            this.guna2Button21.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button21.Name = "guna2Button21";
            this.guna2Button21.Size = new System.Drawing.Size(236, 43);
            this.guna2Button21.TabIndex = 2;
            this.guna2Button21.Text = "View Rubrics";
            this.guna2Button21.Click += new System.EventHandler(this.guna2Button21_Click);
            // 
            // guna2Button22
            // 
            this.guna2Button22.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button22.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button22.BorderRadius = 6;
            this.guna2Button22.BorderThickness = 2;
            this.guna2Button22.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button22.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button22.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button22.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button22.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button22.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button22.ForeColor = System.Drawing.Color.White;
            this.guna2Button22.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button22.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button22.HoverState.Font = new System.Drawing.Font("MV Boli", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button22.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button22.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button22.Location = new System.Drawing.Point(341, 27);
            this.guna2Button22.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button22.Name = "guna2Button22";
            this.guna2Button22.Size = new System.Drawing.Size(234, 43);
            this.guna2Button22.TabIndex = 0;
            this.guna2Button22.Text = "Add Rubric Levels";
            this.guna2Button22.Click += new System.EventHandler(this.guna2Button22_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.pictureBox6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.01245F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.98755F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox6.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox6.Location = new System.Drawing.Point(2, 3);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(251, 95);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.rubricIDBox);
            this.panel3.Controls.Add(this.guna2Button19);
            this.panel3.Controls.Add(this.guna2HtmlLabel9);
            this.panel3.Controls.Add(this.guna2HtmlLabel10);
            this.panel3.Controls.Add(this.pictureBox7);
            this.panel3.Location = new System.Drawing.Point(2, 106);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(253, 497);
            this.panel3.TabIndex = 1;
            // 
            // rubricIDBox
            // 
            this.rubricIDBox.BackColor = System.Drawing.Color.Transparent;
            this.rubricIDBox.BorderRadius = 5;
            this.rubricIDBox.BorderThickness = 3;
            this.rubricIDBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.rubricIDBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rubricIDBox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rubricIDBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.rubricIDBox.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.rubricIDBox.ForeColor = System.Drawing.Color.Black;
            this.rubricIDBox.ItemHeight = 30;
            this.rubricIDBox.Location = new System.Drawing.Point(52, 192);
            this.rubricIDBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rubricIDBox.Name = "rubricIDBox";
            this.rubricIDBox.Size = new System.Drawing.Size(134, 36);
            this.rubricIDBox.TabIndex = 17;
            // 
            // guna2Button19
            // 
            this.guna2Button19.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button19.BorderRadius = 6;
            this.guna2Button19.BorderThickness = 2;
            this.guna2Button19.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button19.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button19.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button19.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button19.FillColor = System.Drawing.Color.SlateGray;
            this.guna2Button19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button19.ForeColor = System.Drawing.Color.White;
            this.guna2Button19.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button19.HoverState.FillColor = System.Drawing.Color.LightSlateGray;
            this.guna2Button19.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button19.Image = global::DB_Mid_Project.Properties.Resources._26_removebg_preview;
            this.guna2Button19.Location = new System.Drawing.Point(56, 428);
            this.guna2Button19.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button19.Name = "guna2Button19";
            this.guna2Button19.Size = new System.Drawing.Size(125, 43);
            this.guna2Button19.TabIndex = 16;
            this.guna2Button19.Text = "Save";
            this.guna2Button19.Click += new System.EventHandler(this.guna2Button19_Click);
            // 
            // guna2HtmlLabel9
            // 
            this.guna2HtmlLabel9.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel9.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel9.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel9.Location = new System.Drawing.Point(8, 71);
            this.guna2HtmlLabel9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel9.Name = "guna2HtmlLabel9";
            this.guna2HtmlLabel9.Size = new System.Drawing.Size(216, 32);
            this.guna2HtmlLabel9.TabIndex = 6;
            this.guna2HtmlLabel9.Text = "Manage Rubric Level";
            // 
            // guna2HtmlLabel10
            // 
            this.guna2HtmlLabel10.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel10.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel10.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel10.Location = new System.Drawing.Point(77, 130);
            this.guna2HtmlLabel10.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel10.Name = "guna2HtmlLabel10";
            this.guna2HtmlLabel10.Size = new System.Drawing.Size(76, 23);
            this.guna2HtmlLabel10.TabIndex = 3;
            this.guna2HtmlLabel10.Text = "Rubric ID:";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox7.Image = global::DB_Mid_Project.Properties.Resources.k8i_removebg_preview;
            this.pictureBox7.Location = new System.Drawing.Point(89, 13);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(52, 52);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // AssessmentTab
            // 
            this.AssessmentTab.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AssessmentTab.Controls.Add(this.guna2GradientPanel4);
            this.AssessmentTab.Controls.Add(this.dataGridView4);
            this.AssessmentTab.Controls.Add(this.tableLayoutPanel5);
            this.AssessmentTab.Location = new System.Drawing.Point(5, 4);
            this.AssessmentTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.AssessmentTab.Name = "AssessmentTab";
            this.AssessmentTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.AssessmentTab.Size = new System.Drawing.Size(892, 612);
            this.AssessmentTab.TabIndex = 9;
            this.AssessmentTab.Text = "tabPage4";
            this.AssessmentTab.UseVisualStyleBackColor = true;
            // 
            // guna2GradientPanel4
            // 
            this.guna2GradientPanel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2GradientPanel4.AutoSize = true;
            this.guna2GradientPanel4.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel4.BorderThickness = 4;
            this.guna2GradientPanel4.Controls.Add(this.guna2Button2);
            this.guna2GradientPanel4.Controls.Add(this.guna2Button20);
            this.guna2GradientPanel4.Controls.Add(this.guna2Button26);
            this.guna2GradientPanel4.Controls.Add(this.guna2Button23);
            this.guna2GradientPanel4.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel4.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel4.Location = new System.Drawing.Point(262, 519);
            this.guna2GradientPanel4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel4.Name = "guna2GradientPanel4";
            this.guna2GradientPanel4.Size = new System.Drawing.Size(631, 97);
            this.guna2GradientPanel4.TabIndex = 6;
            // 
            // guna2Button2
            // 
            this.guna2Button2.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button2.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button2.BorderRadius = 6;
            this.guna2Button2.BorderThickness = 2;
            this.guna2Button2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button2.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button2.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button2.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button2.Image = global::DB_Mid_Project.Properties.Resources._24i_removebg_preview;
            this.guna2Button2.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button2.Location = new System.Drawing.Point(326, 32);
            this.guna2Button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.Size = new System.Drawing.Size(132, 43);
            this.guna2Button2.TabIndex = 8;
            this.guna2Button2.Text = "Update";
            this.guna2Button2.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // guna2Button20
            // 
            this.guna2Button20.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button20.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button20.BorderRadius = 6;
            this.guna2Button20.BorderThickness = 2;
            this.guna2Button20.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button20.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button20.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button20.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button20.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button20.ForeColor = System.Drawing.Color.White;
            this.guna2Button20.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button20.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button20.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button20.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button20.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button20.Location = new System.Drawing.Point(23, 32);
            this.guna2Button20.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button20.Name = "guna2Button20";
            this.guna2Button20.Size = new System.Drawing.Size(128, 43);
            this.guna2Button20.TabIndex = 2;
            this.guna2Button20.Text = "View";
            this.guna2Button20.Click += new System.EventHandler(this.guna2Button20_Click);
            // 
            // guna2Button26
            // 
            this.guna2Button26.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button26.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button26.BorderRadius = 6;
            this.guna2Button26.BorderThickness = 2;
            this.guna2Button26.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button26.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button26.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button26.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button26.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button26.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button26.ForeColor = System.Drawing.Color.White;
            this.guna2Button26.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button26.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button26.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button26.Image = global::DB_Mid_Project.Properties.Resources._23i_removebg_preview__1_;
            this.guna2Button26.ImageSize = new System.Drawing.Size(50, 40);
            this.guna2Button26.Location = new System.Drawing.Point(487, 32);
            this.guna2Button26.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button26.Name = "guna2Button26";
            this.guna2Button26.Size = new System.Drawing.Size(124, 43);
            this.guna2Button26.TabIndex = 7;
            this.guna2Button26.Text = "Delete";
            this.guna2Button26.Click += new System.EventHandler(this.guna2Button26_Click);
            // 
            // guna2Button23
            // 
            this.guna2Button23.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button23.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button23.BorderRadius = 6;
            this.guna2Button23.BorderThickness = 2;
            this.guna2Button23.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button23.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button23.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button23.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button23.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button23.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button23.ForeColor = System.Drawing.Color.White;
            this.guna2Button23.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button23.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button23.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button23.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button23.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button23.Location = new System.Drawing.Point(182, 32);
            this.guna2Button23.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button23.Name = "guna2Button23";
            this.guna2Button23.Size = new System.Drawing.Size(112, 43);
            this.guna2Button23.TabIndex = 0;
            this.guna2Button23.Text = "Add";
            this.guna2Button23.Click += new System.EventHandler(this.guna2Button23_Click);
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(259, 3);
            this.dataGridView4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(631, 606);
            this.dataGridView4.TabIndex = 5;
            this.dataGridView4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.pictureBox8, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.01245F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.98755F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox8.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox8.Location = new System.Drawing.Point(2, 3);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(251, 95);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.DarkCyan;
            this.panel4.Controls.Add(this.id);
            this.panel4.Controls.Add(this.guna2HtmlLabel16);
            this.panel4.Controls.Add(this.guna2Button6);
            this.panel4.Controls.Add(this.tweight);
            this.panel4.Controls.Add(this.guna2HtmlLabel15);
            this.panel4.Controls.Add(this.tmarks);
            this.panel4.Controls.Add(this.dateassesssment);
            this.panel4.Controls.Add(this.guna2DateTimePicker3);
            this.panel4.Controls.Add(this.title);
            this.panel4.Controls.Add(this.guna2HtmlLabel11);
            this.panel4.Controls.Add(this.guna2HtmlLabel12);
            this.panel4.Controls.Add(this.guna2HtmlLabel13);
            this.panel4.Controls.Add(this.guna2HtmlLabel14);
            this.panel4.Controls.Add(this.pictureBox9);
            this.panel4.Location = new System.Drawing.Point(2, 106);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(253, 497);
            this.panel4.TabIndex = 1;
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.BackColor = System.Drawing.Color.MintCream;
            this.id.Location = new System.Drawing.Point(89, 123);
            this.id.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(43, 19);
            this.id.TabIndex = 21;
            this.id.Text = "None";
            // 
            // guna2HtmlLabel16
            // 
            this.guna2HtmlLabel16.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel16.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel16.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel16.Location = new System.Drawing.Point(8, 107);
            this.guna2HtmlLabel16.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel16.Name = "guna2HtmlLabel16";
            this.guna2HtmlLabel16.Size = new System.Drawing.Size(26, 23);
            this.guna2HtmlLabel16.TabIndex = 20;
            this.guna2HtmlLabel16.Text = "ID:";
            // 
            // guna2Button6
            // 
            this.guna2Button6.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button6.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button6.BorderRadius = 6;
            this.guna2Button6.BorderThickness = 2;
            this.guna2Button6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button6.FillColor = System.Drawing.Color.Gray;
            this.guna2Button6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button6.ForeColor = System.Drawing.Color.White;
            this.guna2Button6.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button6.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button6.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button6.Image = global::DB_Mid_Project.Properties.Resources._26_removebg_preview;
            this.guna2Button6.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button6.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button6.Location = new System.Drawing.Point(62, 442);
            this.guna2Button6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button6.Name = "guna2Button6";
            this.guna2Button6.Size = new System.Drawing.Size(114, 43);
            this.guna2Button6.TabIndex = 4;
            this.guna2Button6.Text = "Save";
            // 
            // tweight
            // 
            this.tweight.Animated = true;
            this.tweight.BorderColor = System.Drawing.SystemColors.GrayText;
            this.tweight.BorderRadius = 10;
            this.tweight.BorderThickness = 2;
            this.tweight.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tweight.DefaultText = "";
            this.tweight.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.tweight.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.tweight.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tweight.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tweight.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tweight.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tweight.ForeColor = System.Drawing.Color.Black;
            this.tweight.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tweight.Location = new System.Drawing.Point(20, 399);
            this.tweight.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tweight.Name = "tweight";
            this.tweight.PasswordChar = '\0';
            this.tweight.PlaceholderText = "e.g.  30";
            this.tweight.SelectedText = "";
            this.tweight.Size = new System.Drawing.Size(200, 30);
            this.tweight.TabIndex = 19;
            // 
            // guna2HtmlLabel15
            // 
            this.guna2HtmlLabel15.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel15.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel15.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel15.Location = new System.Drawing.Point(8, 368);
            this.guna2HtmlLabel15.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel15.Name = "guna2HtmlLabel15";
            this.guna2HtmlLabel15.Size = new System.Drawing.Size(143, 27);
            this.guna2HtmlLabel15.TabIndex = 18;
            this.guna2HtmlLabel15.Text = "Total Weightage:";
            // 
            // tmarks
            // 
            this.tmarks.Animated = true;
            this.tmarks.BorderColor = System.Drawing.SystemColors.GrayText;
            this.tmarks.BorderRadius = 10;
            this.tmarks.BorderThickness = 2;
            this.tmarks.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tmarks.DefaultText = "";
            this.tmarks.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.tmarks.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.tmarks.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tmarks.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tmarks.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tmarks.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tmarks.ForeColor = System.Drawing.Color.Black;
            this.tmarks.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tmarks.Location = new System.Drawing.Point(22, 338);
            this.tmarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tmarks.Name = "tmarks";
            this.tmarks.PasswordChar = '\0';
            this.tmarks.PlaceholderText = "e.g.  30";
            this.tmarks.SelectedText = "";
            this.tmarks.Size = new System.Drawing.Size(200, 30);
            this.tmarks.TabIndex = 17;
            // 
            // dateassesssment
            // 
            this.dateassesssment.Animated = true;
            this.dateassesssment.BorderColor = System.Drawing.SystemColors.GrayText;
            this.dateassesssment.BorderRadius = 10;
            this.dateassesssment.BorderThickness = 2;
            this.dateassesssment.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dateassesssment.DefaultText = "";
            this.dateassesssment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.dateassesssment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.dateassesssment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateassesssment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateassesssment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateassesssment.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dateassesssment.ForeColor = System.Drawing.Color.Black;
            this.dateassesssment.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateassesssment.Location = new System.Drawing.Point(20, 240);
            this.dateassesssment.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dateassesssment.Name = "dateassesssment";
            this.dateassesssment.PasswordChar = '\0';
            this.dateassesssment.PlaceholderText = "yyyy-dd-mm";
            this.dateassesssment.ReadOnly = true;
            this.dateassesssment.SelectedText = "";
            this.dateassesssment.Size = new System.Drawing.Size(200, 30);
            this.dateassesssment.TabIndex = 15;
            // 
            // guna2DateTimePicker3
            // 
            this.guna2DateTimePicker3.Checked = true;
            this.guna2DateTimePicker3.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker3.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker3.Location = new System.Drawing.Point(20, 277);
            this.guna2DateTimePicker3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker3.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker3.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker3.Name = "guna2DateTimePicker3";
            this.guna2DateTimePicker3.Size = new System.Drawing.Size(200, 30);
            this.guna2DateTimePicker3.TabIndex = 13;
            this.guna2DateTimePicker3.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker3.ValueChanged += new System.EventHandler(this.guna2DateTimePicker3_ValueChanged);
            // 
            // title
            // 
            this.title.Animated = true;
            this.title.BorderColor = System.Drawing.SystemColors.GrayText;
            this.title.BorderRadius = 10;
            this.title.BorderThickness = 2;
            this.title.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.title.DefaultText = "";
            this.title.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.title.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.title.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.title.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.title.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.title.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.title.ForeColor = System.Drawing.Color.Black;
            this.title.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.title.Location = new System.Drawing.Point(20, 178);
            this.title.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.title.Name = "title";
            this.title.PasswordChar = '\0';
            this.title.PlaceholderText = "2022-CS-XX";
            this.title.SelectedText = "";
            this.title.Size = new System.Drawing.Size(200, 30);
            this.title.TabIndex = 12;
            // 
            // guna2HtmlLabel11
            // 
            this.guna2HtmlLabel11.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel11.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel11.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel11.Location = new System.Drawing.Point(23, 53);
            this.guna2HtmlLabel11.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel11.Name = "guna2HtmlLabel11";
            this.guna2HtmlLabel11.Size = new System.Drawing.Size(214, 32);
            this.guna2HtmlLabel11.TabIndex = 6;
            this.guna2HtmlLabel11.Text = "Manage Assessment";
            // 
            // guna2HtmlLabel12
            // 
            this.guna2HtmlLabel12.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel12.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel12.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel12.Location = new System.Drawing.Point(8, 149);
            this.guna2HtmlLabel12.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel12.Name = "guna2HtmlLabel12";
            this.guna2HtmlLabel12.Size = new System.Drawing.Size(41, 23);
            this.guna2HtmlLabel12.TabIndex = 3;
            this.guna2HtmlLabel12.Text = "Title:";
            // 
            // guna2HtmlLabel13
            // 
            this.guna2HtmlLabel13.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel13.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel13.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel13.Location = new System.Drawing.Point(8, 309);
            this.guna2HtmlLabel13.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel13.Name = "guna2HtmlLabel13";
            this.guna2HtmlLabel13.Size = new System.Drawing.Size(106, 27);
            this.guna2HtmlLabel13.TabIndex = 2;
            this.guna2HtmlLabel13.Text = "Total Marks:";
            // 
            // guna2HtmlLabel14
            // 
            this.guna2HtmlLabel14.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel14.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel14.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel14.Location = new System.Drawing.Point(8, 211);
            this.guna2HtmlLabel14.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel14.Name = "guna2HtmlLabel14";
            this.guna2HtmlLabel14.Size = new System.Drawing.Size(43, 23);
            this.guna2HtmlLabel14.TabIndex = 0;
            this.guna2HtmlLabel14.Text = "Date:";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox9.Image = global::DB_Mid_Project.Properties.Resources.k11i_removebg_preview;
            this.pictureBox9.Location = new System.Drawing.Point(106, 1);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(49, 52);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 7;
            this.pictureBox9.TabStop = false;
            // 
            // CloTab
            // 
            this.CloTab.BackColor = System.Drawing.SystemColors.ControlDark;
            this.CloTab.Controls.Add(this.guna2CustomGradientPanel1);
            this.CloTab.Controls.Add(this.tableLayoutPanel1);
            this.CloTab.Controls.Add(this.dataGridView1);
            this.CloTab.Location = new System.Drawing.Point(5, 4);
            this.CloTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CloTab.Name = "CloTab";
            this.CloTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CloTab.Size = new System.Drawing.Size(892, 612);
            this.CloTab.TabIndex = 10;
            this.CloTab.Text = "tabPage1";
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.BackColor = System.Drawing.Color.DarkCyan;
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2Button3);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2Button4);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2Button1);
            this.guna2CustomGradientPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.Transparent;
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.Transparent;
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.Transparent;
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.Transparent;
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(259, 492);
            this.guna2CustomGradientPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(631, 117);
            this.guna2CustomGradientPanel1.TabIndex = 2;
            // 
            // guna2Button3
            // 
            this.guna2Button3.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button3.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button3.BorderRadius = 6;
            this.guna2Button3.BorderThickness = 2;
            this.guna2Button3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button3.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button3.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button3.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button3.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.guna2Button3.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button3.Location = new System.Drawing.Point(428, 51);
            this.guna2Button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.Size = new System.Drawing.Size(180, 43);
            this.guna2Button3.TabIndex = 14;
            this.guna2Button3.Text = "Update CLO";
            this.guna2Button3.Click += new System.EventHandler(this.guna2Button3_Click);
            // 
            // guna2Button4
            // 
            this.guna2Button4.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button4.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button4.BorderRadius = 6;
            this.guna2Button4.BorderThickness = 2;
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button4.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button4.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button4.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button4.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button4.Location = new System.Drawing.Point(35, 51);
            this.guna2Button4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.Size = new System.Drawing.Size(146, 43);
            this.guna2Button4.TabIndex = 13;
            this.guna2Button4.Text = "View CLOs";
            this.guna2Button4.Click += new System.EventHandler(this.guna2Button4_Click);
            // 
            // guna2Button1
            // 
            this.guna2Button1.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button1.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button1.BorderRadius = 6;
            this.guna2Button1.BorderThickness = 2;
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button1.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button1.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button1.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button1.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button1.Location = new System.Drawing.Point(224, 51);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Size = new System.Drawing.Size(154, 43);
            this.guna2Button1.TabIndex = 11;
            this.guna2Button1.Text = "Add CLO";
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.01245F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.98755F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox1.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox1.Location = new System.Drawing.Point(2, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.guna2DateTimePicker2);
            this.panel1.Controls.Add(this.guna2DateTimePicker1);
            this.panel1.Controls.Add(this.guna2TextBox3);
            this.panel1.Controls.Add(this.guna2TextBox2);
            this.panel1.Controls.Add(this.guna2TextBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.guna2HtmlLabel7);
            this.panel1.Controls.Add(this.guna2HtmlLabel6);
            this.panel1.Controls.Add(this.guna2HtmlLabel4);
            this.panel1.Controls.Add(this.guna2HtmlLabel1);
            this.panel1.Controls.Add(this.guna2Button16);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(2, 106);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 497);
            this.panel1.TabIndex = 1;
            // 
            // guna2DateTimePicker2
            // 
            this.guna2DateTimePicker2.Checked = true;
            this.guna2DateTimePicker2.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker2.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker2.Location = new System.Drawing.Point(26, 347);
            this.guna2DateTimePicker2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker2.Name = "guna2DateTimePicker2";
            this.guna2DateTimePicker2.Size = new System.Drawing.Size(196, 29);
            this.guna2DateTimePicker2.TabIndex = 27;
            this.guna2DateTimePicker2.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker2.ValueChanged += new System.EventHandler(this.guna2DateTimePicker2_ValueChanged);
            // 
            // guna2DateTimePicker1
            // 
            this.guna2DateTimePicker1.Checked = true;
            this.guna2DateTimePicker1.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker1.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker1.Location = new System.Drawing.Point(29, 204);
            this.guna2DateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker1.Name = "guna2DateTimePicker1";
            this.guna2DateTimePicker1.Size = new System.Drawing.Size(196, 29);
            this.guna2DateTimePicker1.TabIndex = 26;
            this.guna2DateTimePicker1.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker1.ValueChanged += new System.EventHandler(this.guna2DateTimePicker1_ValueChanged_1);
            // 
            // guna2TextBox3
            // 
            this.guna2TextBox3.Animated = true;
            this.guna2TextBox3.BorderColor = System.Drawing.SystemColors.GrayText;
            this.guna2TextBox3.BorderRadius = 10;
            this.guna2TextBox3.BorderThickness = 2;
            this.guna2TextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox3.DefaultText = "";
            this.guna2TextBox3.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox3.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Location = new System.Drawing.Point(25, 311);
            this.guna2TextBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2TextBox3.Name = "guna2TextBox3";
            this.guna2TextBox3.PasswordChar = '\0';
            this.guna2TextBox3.PlaceholderText = "2024/07/03";
            this.guna2TextBox3.SelectedText = "";
            this.guna2TextBox3.Size = new System.Drawing.Size(200, 30);
            this.guna2TextBox3.TabIndex = 10;
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.Animated = true;
            this.guna2TextBox2.BorderColor = System.Drawing.SystemColors.GrayText;
            this.guna2TextBox2.BorderRadius = 10;
            this.guna2TextBox2.BorderThickness = 2;
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Location = new System.Drawing.Point(25, 169);
            this.guna2TextBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderText = "2024/05/03";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.Size = new System.Drawing.Size(200, 30);
            this.guna2TextBox2.TabIndex = 9;
            // 
            // guna2TextBox1
            // 
            this.guna2TextBox1.Animated = true;
            this.guna2TextBox1.BorderColor = System.Drawing.SystemColors.GrayText;
            this.guna2TextBox1.BorderRadius = 10;
            this.guna2TextBox1.BorderThickness = 2;
            this.guna2TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox1.DefaultText = "";
            this.guna2TextBox1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2TextBox1.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.Location = new System.Drawing.Point(25, 100);
            this.guna2TextBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2TextBox1.Name = "guna2TextBox1";
            this.guna2TextBox1.PasswordChar = '\0';
            this.guna2TextBox1.PlaceholderText = "e.g. CLO1";
            this.guna2TextBox1.SelectedText = "";
            this.guna2TextBox1.Size = new System.Drawing.Size(200, 30);
            this.guna2TextBox1.TabIndex = 8;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox2.Image = global::DB_Mid_Project.Properties.Resources.k8i_removebg_preview;
            this.pictureBox2.Location = new System.Drawing.Point(6, 9);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(46, 45);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // guna2HtmlLabel7
            // 
            this.guna2HtmlLabel7.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel7.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel7.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel7.Location = new System.Drawing.Point(65, 16);
            this.guna2HtmlLabel7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel7.Name = "guna2HtmlLabel7";
            this.guna2HtmlLabel7.Size = new System.Drawing.Size(142, 32);
            this.guna2HtmlLabel7.TabIndex = 6;
            this.guna2HtmlLabel7.Text = "Manage CLOs";
            // 
            // guna2HtmlLabel6
            // 
            this.guna2HtmlLabel6.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel6.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel6.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel6.Location = new System.Drawing.Point(8, 282);
            this.guna2HtmlLabel6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel6.Name = "guna2HtmlLabel6";
            this.guna2HtmlLabel6.Size = new System.Drawing.Size(110, 23);
            this.guna2HtmlLabel6.TabIndex = 5;
            this.guna2HtmlLabel6.Text = "Date Updated:";
            // 
            // guna2HtmlLabel4
            // 
            this.guna2HtmlLabel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel4.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel4.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel4.Location = new System.Drawing.Point(8, 139);
            this.guna2HtmlLabel4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel4.Name = "guna2HtmlLabel4";
            this.guna2HtmlLabel4.Size = new System.Drawing.Size(105, 23);
            this.guna2HtmlLabel4.TabIndex = 3;
            this.guna2HtmlLabel4.Text = "Date Created:";
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel1.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(8, 71);
            this.guna2HtmlLabel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(91, 23);
            this.guna2HtmlLabel1.TabIndex = 0;
            this.guna2HtmlLabel1.Text = "CLO Name:";
            // 
            // guna2Button16
            // 
            this.guna2Button16.BackColor = System.Drawing.Color.LightSlateGray;
            this.guna2Button16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.guna2Button16.BorderRadius = 6;
            this.guna2Button16.BorderThickness = 2;
            this.guna2Button16.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button16.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button16.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button16.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button16.FillColor = System.Drawing.Color.SlateGray;
            this.guna2Button16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button16.ForeColor = System.Drawing.Color.White;
            this.guna2Button16.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button16.HoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button16.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button16.Image = global::DB_Mid_Project.Properties.Resources._26_removebg_preview;
            this.guna2Button16.Location = new System.Drawing.Point(55, 441);
            this.guna2Button16.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button16.Name = "guna2Button16";
            this.guna2Button16.Size = new System.Drawing.Size(115, 36);
            this.guna2Button16.TabIndex = 25;
            this.guna2Button16.Text = "Save";
            this.guna2Button16.Click += new System.EventHandler(this.guna2Button16_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(258, -4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(638, 495);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // AssessmentComponentTab
            // 
            this.AssessmentComponentTab.Controls.Add(this.guna2GradientPanel5);
            this.AssessmentComponentTab.Controls.Add(this.dataGridView5);
            this.AssessmentComponentTab.Controls.Add(this.tableLayoutPanel6);
            this.AssessmentComponentTab.Location = new System.Drawing.Point(5, 4);
            this.AssessmentComponentTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.AssessmentComponentTab.Name = "AssessmentComponentTab";
            this.AssessmentComponentTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.AssessmentComponentTab.Size = new System.Drawing.Size(892, 612);
            this.AssessmentComponentTab.TabIndex = 11;
            this.AssessmentComponentTab.Text = "tabPage5";
            this.AssessmentComponentTab.UseVisualStyleBackColor = true;
            // 
            // guna2GradientPanel5
            // 
            this.guna2GradientPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.guna2GradientPanel5.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel5.BorderThickness = 4;
            this.guna2GradientPanel5.Controls.Add(this.guna2Button27);
            this.guna2GradientPanel5.Controls.Add(this.guna2Button28);
            this.guna2GradientPanel5.Controls.Add(this.guna2Button29);
            this.guna2GradientPanel5.Controls.Add(this.guna2Button30);
            this.guna2GradientPanel5.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel5.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel5.Location = new System.Drawing.Point(260, 508);
            this.guna2GradientPanel5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel5.Name = "guna2GradientPanel5";
            this.guna2GradientPanel5.Size = new System.Drawing.Size(629, 100);
            this.guna2GradientPanel5.TabIndex = 6;
            // 
            // guna2Button27
            // 
            this.guna2Button27.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button27.BorderColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button27.BorderRadius = 6;
            this.guna2Button27.BorderThickness = 2;
            this.guna2Button27.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button27.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button27.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button27.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button27.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button27.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button27.ForeColor = System.Drawing.Color.White;
            this.guna2Button27.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button27.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button27.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button27.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button27.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button27.Location = new System.Drawing.Point(17, 32);
            this.guna2Button27.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button27.Name = "guna2Button27";
            this.guna2Button27.Size = new System.Drawing.Size(122, 43);
            this.guna2Button27.TabIndex = 3;
            this.guna2Button27.Text = "View";
            this.guna2Button27.Click += new System.EventHandler(this.guna2Button27_Click);
            // 
            // guna2Button28
            // 
            this.guna2Button28.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button28.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button28.BorderRadius = 6;
            this.guna2Button28.BorderThickness = 2;
            this.guna2Button28.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button28.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button28.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button28.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button28.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button28.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button28.ForeColor = System.Drawing.Color.White;
            this.guna2Button28.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button28.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button28.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button28.Image = global::DB_Mid_Project.Properties.Resources._24i_removebg_preview;
            this.guna2Button28.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button28.Location = new System.Drawing.Point(328, 33);
            this.guna2Button28.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button28.Name = "guna2Button28";
            this.guna2Button28.Size = new System.Drawing.Size(112, 43);
            this.guna2Button28.TabIndex = 2;
            this.guna2Button28.Text = "Edit";
            this.guna2Button28.Click += new System.EventHandler(this.guna2Button28_Click);
            // 
            // guna2Button29
            // 
            this.guna2Button29.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button29.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button29.BorderRadius = 6;
            this.guna2Button29.BorderThickness = 2;
            this.guna2Button29.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button29.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button29.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button29.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button29.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button29.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button29.ForeColor = System.Drawing.Color.White;
            this.guna2Button29.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button29.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button29.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button29.Image = global::DB_Mid_Project.Properties.Resources._23i_removebg_preview__1_;
            this.guna2Button29.ImageSize = new System.Drawing.Size(40, 35);
            this.guna2Button29.Location = new System.Drawing.Point(466, 33);
            this.guna2Button29.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button29.Name = "guna2Button29";
            this.guna2Button29.Size = new System.Drawing.Size(106, 43);
            this.guna2Button29.TabIndex = 1;
            this.guna2Button29.Text = "Delete";
            this.guna2Button29.Click += new System.EventHandler(this.guna2Button29_Click);
            // 
            // guna2Button30
            // 
            this.guna2Button30.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button30.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button30.BorderRadius = 6;
            this.guna2Button30.BorderThickness = 2;
            this.guna2Button30.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button30.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button30.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button30.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button30.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button30.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button30.ForeColor = System.Drawing.Color.White;
            this.guna2Button30.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button30.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button30.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button30.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button30.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button30.Location = new System.Drawing.Point(173, 33);
            this.guna2Button30.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button30.Name = "guna2Button30";
            this.guna2Button30.Size = new System.Drawing.Size(119, 43);
            this.guna2Button30.TabIndex = 0;
            this.guna2Button30.Text = "Add";
            this.guna2Button30.Click += new System.EventHandler(this.guna2Button30_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView5.Location = new System.Drawing.Point(259, 3);
            this.dataGridView5.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.ReadOnly = true;
            this.dataGridView5.Size = new System.Drawing.Size(631, 606);
            this.dataGridView5.TabIndex = 5;
            this.dataGridView5.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView5_CellContentClick);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.pictureBox10, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.01245F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.98755F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox10.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox10.Location = new System.Drawing.Point(2, 3);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(251, 95);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 0;
            this.pictureBox10.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.DarkCyan;
            this.panel5.Controls.Add(this.ACId);
            this.panel5.Controls.Add(this.compoMarks);
            this.panel5.Controls.Add(this.assessmentidComboBox);
            this.panel5.Controls.Add(this.guna2HtmlLabel24);
            this.panel5.Controls.Add(this.DateCreated);
            this.panel5.Controls.Add(this.guna2DateTimePicker5);
            this.panel5.Controls.Add(this.guna2HtmlLabel22);
            this.panel5.Controls.Add(this.guna2HtmlLabel21);
            this.panel5.Controls.Add(this.dateUpdated);
            this.panel5.Controls.Add(this.RidcomboBox);
            this.panel5.Controls.Add(this.guna2DateTimePicker4);
            this.panel5.Controls.Add(this.componentname);
            this.panel5.Controls.Add(this.guna2HtmlLabel17);
            this.panel5.Controls.Add(this.guna2HtmlLabel18);
            this.panel5.Controls.Add(this.guna2HtmlLabel19);
            this.panel5.Controls.Add(this.guna2HtmlLabel20);
            this.panel5.Controls.Add(this.pictureBox11);
            this.panel5.Location = new System.Drawing.Point(2, 106);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(253, 497);
            this.panel5.TabIndex = 1;
            // 
            // ACId
            // 
            this.ACId.AutoSize = true;
            this.ACId.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ACId.ForeColor = System.Drawing.Color.Maroon;
            this.ACId.Location = new System.Drawing.Point(226, 107);
            this.ACId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ACId.Name = "ACId";
            this.ACId.Size = new System.Drawing.Size(23, 21);
            this.ACId.TabIndex = 25;
            this.ACId.Text = "N";
            // 
            // compoMarks
            // 
            this.compoMarks.Animated = true;
            this.compoMarks.BorderColor = System.Drawing.SystemColors.GrayText;
            this.compoMarks.BorderRadius = 10;
            this.compoMarks.BorderThickness = 2;
            this.compoMarks.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.compoMarks.DefaultText = "";
            this.compoMarks.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.compoMarks.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.compoMarks.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.compoMarks.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.compoMarks.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.compoMarks.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.compoMarks.ForeColor = System.Drawing.Color.Black;
            this.compoMarks.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.compoMarks.Location = new System.Drawing.Point(130, 225);
            this.compoMarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.compoMarks.Name = "compoMarks";
            this.compoMarks.PasswordChar = '\0';
            this.compoMarks.PlaceholderText = "e.g. 05";
            this.compoMarks.SelectedText = "";
            this.compoMarks.Size = new System.Drawing.Size(90, 30);
            this.compoMarks.TabIndex = 24;
            // 
            // assessmentidComboBox
            // 
            this.assessmentidComboBox.BackColor = System.Drawing.Color.Transparent;
            this.assessmentidComboBox.BorderRadius = 5;
            this.assessmentidComboBox.BorderThickness = 3;
            this.assessmentidComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.assessmentidComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.assessmentidComboBox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.assessmentidComboBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.assessmentidComboBox.Font = new System.Drawing.Font("Segoe Print", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentidComboBox.ForeColor = System.Drawing.Color.Black;
            this.assessmentidComboBox.ItemHeight = 30;
            this.assessmentidComboBox.Location = new System.Drawing.Point(155, 179);
            this.assessmentidComboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.assessmentidComboBox.Name = "assessmentidComboBox";
            this.assessmentidComboBox.Size = new System.Drawing.Size(65, 36);
            this.assessmentidComboBox.TabIndex = 23;
            // 
            // guna2HtmlLabel24
            // 
            this.guna2HtmlLabel24.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel24.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel24.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel24.Location = new System.Drawing.Point(10, 192);
            this.guna2HtmlLabel24.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel24.Name = "guna2HtmlLabel24";
            this.guna2HtmlLabel24.Size = new System.Drawing.Size(105, 23);
            this.guna2HtmlLabel24.TabIndex = 22;
            this.guna2HtmlLabel24.Text = "Assessment Id:";
            // 
            // DateCreated
            // 
            this.DateCreated.Animated = true;
            this.DateCreated.BorderColor = System.Drawing.SystemColors.GrayText;
            this.DateCreated.BorderRadius = 10;
            this.DateCreated.BorderThickness = 2;
            this.DateCreated.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.DateCreated.DefaultText = "";
            this.DateCreated.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.DateCreated.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.DateCreated.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.DateCreated.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.DateCreated.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.DateCreated.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.DateCreated.ForeColor = System.Drawing.Color.Black;
            this.DateCreated.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.DateCreated.Location = new System.Drawing.Point(20, 293);
            this.DateCreated.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.DateCreated.Name = "DateCreated";
            this.DateCreated.PasswordChar = '\0';
            this.DateCreated.PlaceholderText = "yyyy-dd-mm";
            this.DateCreated.ReadOnly = true;
            this.DateCreated.SelectedText = "";
            this.DateCreated.Size = new System.Drawing.Size(200, 30);
            this.DateCreated.TabIndex = 20;
            // 
            // guna2DateTimePicker5
            // 
            this.guna2DateTimePicker5.Checked = true;
            this.guna2DateTimePicker5.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker5.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker5.Location = new System.Drawing.Point(20, 329);
            this.guna2DateTimePicker5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker5.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker5.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker5.Name = "guna2DateTimePicker5";
            this.guna2DateTimePicker5.Size = new System.Drawing.Size(200, 30);
            this.guna2DateTimePicker5.TabIndex = 19;
            this.guna2DateTimePicker5.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker5.ValueChanged += new System.EventHandler(this.guna2DateTimePicker5_ValueChanged);
            // 
            // guna2HtmlLabel22
            // 
            this.guna2HtmlLabel22.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel22.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel22.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel22.Location = new System.Drawing.Point(8, 264);
            this.guna2HtmlLabel22.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel22.Name = "guna2HtmlLabel22";
            this.guna2HtmlLabel22.Size = new System.Drawing.Size(105, 23);
            this.guna2HtmlLabel22.TabIndex = 18;
            this.guna2HtmlLabel22.Text = "Date Created:";
            // 
            // guna2HtmlLabel21
            // 
            this.guna2HtmlLabel21.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel21.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel21.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel21.Location = new System.Drawing.Point(8, 149);
            this.guna2HtmlLabel21.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel21.Name = "guna2HtmlLabel21";
            this.guna2HtmlLabel21.Size = new System.Drawing.Size(70, 23);
            this.guna2HtmlLabel21.TabIndex = 17;
            this.guna2HtmlLabel21.Text = "Rubric Id:";
            // 
            // dateUpdated
            // 
            this.dateUpdated.Animated = true;
            this.dateUpdated.BorderColor = System.Drawing.SystemColors.GrayText;
            this.dateUpdated.BorderRadius = 10;
            this.dateUpdated.BorderThickness = 2;
            this.dateUpdated.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dateUpdated.DefaultText = "";
            this.dateUpdated.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.dateUpdated.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.dateUpdated.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateUpdated.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateUpdated.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateUpdated.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dateUpdated.ForeColor = System.Drawing.Color.Black;
            this.dateUpdated.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateUpdated.Location = new System.Drawing.Point(20, 410);
            this.dateUpdated.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dateUpdated.Name = "dateUpdated";
            this.dateUpdated.PasswordChar = '\0';
            this.dateUpdated.PlaceholderText = "yyyy-dd-mm";
            this.dateUpdated.ReadOnly = true;
            this.dateUpdated.SelectedText = "";
            this.dateUpdated.Size = new System.Drawing.Size(200, 30);
            this.dateUpdated.TabIndex = 15;
            // 
            // RidcomboBox
            // 
            this.RidcomboBox.BackColor = System.Drawing.Color.Transparent;
            this.RidcomboBox.BorderRadius = 5;
            this.RidcomboBox.BorderThickness = 3;
            this.RidcomboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.RidcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RidcomboBox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RidcomboBox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RidcomboBox.Font = new System.Drawing.Font("Segoe Print", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RidcomboBox.ForeColor = System.Drawing.Color.Black;
            this.RidcomboBox.ItemHeight = 30;
            this.RidcomboBox.Location = new System.Drawing.Point(155, 136);
            this.RidcomboBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RidcomboBox.Name = "RidcomboBox";
            this.RidcomboBox.Size = new System.Drawing.Size(65, 36);
            this.RidcomboBox.TabIndex = 14;
            // 
            // guna2DateTimePicker4
            // 
            this.guna2DateTimePicker4.Checked = true;
            this.guna2DateTimePicker4.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker4.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker4.Location = new System.Drawing.Point(20, 446);
            this.guna2DateTimePicker4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker4.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker4.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker4.Name = "guna2DateTimePicker4";
            this.guna2DateTimePicker4.Size = new System.Drawing.Size(200, 30);
            this.guna2DateTimePicker4.TabIndex = 13;
            this.guna2DateTimePicker4.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker4.ValueChanged += new System.EventHandler(this.guna2DateTimePicker4_ValueChanged);
            // 
            // componentname
            // 
            this.componentname.Animated = true;
            this.componentname.BorderColor = System.Drawing.SystemColors.GrayText;
            this.componentname.BorderRadius = 10;
            this.componentname.BorderThickness = 2;
            this.componentname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.componentname.DefaultText = "";
            this.componentname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.componentname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.componentname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.componentname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.componentname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.componentname.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.componentname.ForeColor = System.Drawing.Color.Black;
            this.componentname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.componentname.Location = new System.Drawing.Point(79, 100);
            this.componentname.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.componentname.Name = "componentname";
            this.componentname.PasswordChar = '\0';
            this.componentname.PlaceholderText = "e.g. Question 1";
            this.componentname.SelectedText = "";
            this.componentname.Size = new System.Drawing.Size(140, 30);
            this.componentname.TabIndex = 12;
            // 
            // guna2HtmlLabel17
            // 
            this.guna2HtmlLabel17.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel17.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel17.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel17.Location = new System.Drawing.Point(0, 53);
            this.guna2HtmlLabel17.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel17.Name = "guna2HtmlLabel17";
            this.guna2HtmlLabel17.Size = new System.Drawing.Size(250, 32);
            this.guna2HtmlLabel17.TabIndex = 6;
            this.guna2HtmlLabel17.Text = "Assessment Component";
            // 
            // guna2HtmlLabel18
            // 
            this.guna2HtmlLabel18.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel18.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel18.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel18.Location = new System.Drawing.Point(10, 107);
            this.guna2HtmlLabel18.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel18.Name = "guna2HtmlLabel18";
            this.guna2HtmlLabel18.Size = new System.Drawing.Size(46, 23);
            this.guna2HtmlLabel18.TabIndex = 3;
            this.guna2HtmlLabel18.Text = "Name:";
            // 
            // guna2HtmlLabel19
            // 
            this.guna2HtmlLabel19.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel19.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel19.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel19.Location = new System.Drawing.Point(8, 230);
            this.guna2HtmlLabel19.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel19.Name = "guna2HtmlLabel19";
            this.guna2HtmlLabel19.Size = new System.Drawing.Size(106, 27);
            this.guna2HtmlLabel19.TabIndex = 2;
            this.guna2HtmlLabel19.Text = "Total Marks:";
            // 
            // guna2HtmlLabel20
            // 
            this.guna2HtmlLabel20.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel20.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel20.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel20.Location = new System.Drawing.Point(8, 381);
            this.guna2HtmlLabel20.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel20.Name = "guna2HtmlLabel20";
            this.guna2HtmlLabel20.Size = new System.Drawing.Size(110, 23);
            this.guna2HtmlLabel20.TabIndex = 0;
            this.guna2HtmlLabel20.Text = "Date Updated:";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox11.Image = global::DB_Mid_Project.Properties.Resources.k7i_removebg_preview;
            this.pictureBox11.Location = new System.Drawing.Point(106, 1);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(49, 52);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 7;
            this.pictureBox11.TabStop = false;
            // 
            // ResultTab
            // 
            this.ResultTab.Controls.Add(this.guna2GradientPanel6);
            this.ResultTab.Controls.Add(this.dataGridView6);
            this.ResultTab.Controls.Add(this.tableLayoutPanel7);
            this.ResultTab.Location = new System.Drawing.Point(5, 4);
            this.ResultTab.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ResultTab.Name = "ResultTab";
            this.ResultTab.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ResultTab.Size = new System.Drawing.Size(892, 612);
            this.ResultTab.TabIndex = 12;
            this.ResultTab.Text = "tabPage6";
            this.ResultTab.UseVisualStyleBackColor = true;
            // 
            // guna2GradientPanel6
            // 
            this.guna2GradientPanel6.AutoSize = true;
            this.guna2GradientPanel6.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel6.BorderThickness = 4;
            this.guna2GradientPanel6.Controls.Add(this.guna2Button31);
            this.guna2GradientPanel6.Controls.Add(this.guna2Button34);
            this.guna2GradientPanel6.Controls.Add(this.guna2Button32);
            this.guna2GradientPanel6.Controls.Add(this.guna2Button33);
            this.guna2GradientPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientPanel6.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel6.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel6.Location = new System.Drawing.Point(259, 517);
            this.guna2GradientPanel6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel6.Name = "guna2GradientPanel6";
            this.guna2GradientPanel6.Size = new System.Drawing.Size(631, 92);
            this.guna2GradientPanel6.TabIndex = 6;
            // 
            // guna2Button31
            // 
            this.guna2Button31.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button31.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button31.BorderRadius = 6;
            this.guna2Button31.BorderThickness = 2;
            this.guna2Button31.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button31.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button31.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button31.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button31.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button31.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button31.ForeColor = System.Drawing.Color.White;
            this.guna2Button31.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button31.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button31.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button31.Image = global::DB_Mid_Project.Properties.Resources._24i_removebg_preview;
            this.guna2Button31.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button31.Location = new System.Drawing.Point(337, 32);
            this.guna2Button31.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button31.Name = "guna2Button31";
            this.guna2Button31.Size = new System.Drawing.Size(106, 43);
            this.guna2Button31.TabIndex = 4;
            this.guna2Button31.Text = "Edit";
            this.guna2Button31.Click += new System.EventHandler(this.guna2Button31_Click);
            // 
            // guna2Button34
            // 
            this.guna2Button34.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button34.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button34.BorderRadius = 6;
            this.guna2Button34.BorderThickness = 2;
            this.guna2Button34.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button34.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button34.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button34.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button34.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button34.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button34.ForeColor = System.Drawing.Color.White;
            this.guna2Button34.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button34.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button34.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button34.Image = global::DB_Mid_Project.Properties.Resources._23i_removebg_preview__1_;
            this.guna2Button34.ImageSize = new System.Drawing.Size(50, 40);
            this.guna2Button34.Location = new System.Drawing.Point(488, 32);
            this.guna2Button34.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button34.Name = "guna2Button34";
            this.guna2Button34.Size = new System.Drawing.Size(122, 43);
            this.guna2Button34.TabIndex = 3;
            this.guna2Button34.Text = "Remove ";
            this.guna2Button34.Click += new System.EventHandler(this.guna2Button34_Click);
            // 
            // guna2Button32
            // 
            this.guna2Button32.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button32.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button32.BorderRadius = 6;
            this.guna2Button32.BorderThickness = 2;
            this.guna2Button32.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button32.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button32.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button32.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button32.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button32.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button32.ForeColor = System.Drawing.Color.White;
            this.guna2Button32.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button32.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button32.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button32.Image = global::DB_Mid_Project.Properties.Resources._25_removebg_preview;
            this.guna2Button32.ImageSize = new System.Drawing.Size(30, 20);
            this.guna2Button32.Location = new System.Drawing.Point(23, 32);
            this.guna2Button32.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button32.Name = "guna2Button32";
            this.guna2Button32.Size = new System.Drawing.Size(100, 43);
            this.guna2Button32.TabIndex = 2;
            this.guna2Button32.Text = "View ";
            this.guna2Button32.Click += new System.EventHandler(this.guna2Button32_Click);
            // 
            // guna2Button33
            // 
            this.guna2Button33.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button33.BorderColor = System.Drawing.Color.Teal;
            this.guna2Button33.BorderRadius = 6;
            this.guna2Button33.BorderThickness = 2;
            this.guna2Button33.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button33.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button33.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button33.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button33.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button33.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button33.ForeColor = System.Drawing.Color.White;
            this.guna2Button33.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button33.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button33.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button33.Image = global::DB_Mid_Project.Properties.Resources.k19i_removebg_preview;
            this.guna2Button33.ImageSize = new System.Drawing.Size(25, 25);
            this.guna2Button33.Location = new System.Drawing.Point(179, 32);
            this.guna2Button33.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button33.Name = "guna2Button33";
            this.guna2Button33.Size = new System.Drawing.Size(109, 43);
            this.guna2Button33.TabIndex = 0;
            this.guna2Button33.Text = "Add";
            this.guna2Button33.Click += new System.EventHandler(this.guna2Button33_Click);
            // 
            // dataGridView6
            // 
            this.dataGridView6.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView6.Location = new System.Drawing.Point(259, 3);
            this.dataGridView6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.ReadOnly = true;
            this.dataGridView6.Size = new System.Drawing.Size(631, 514);
            this.dataGridView6.TabIndex = 5;
            this.dataGridView6.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView6_CellContentClick);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.pictureBox12, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(2, 3);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.16529F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.83471F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(257, 606);
            this.tableLayoutPanel7.TabIndex = 4;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox12.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox12.Location = new System.Drawing.Point(2, 3);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(253, 116);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.DarkCyan;
            this.panel6.Controls.Add(this.resultId);
            this.panel6.Controls.Add(this.acidcombobox);
            this.panel6.Controls.Add(this.RMidcombobox);
            this.panel6.Controls.Add(this.guna2HtmlLabel28);
            this.panel6.Controls.Add(this.dateEvaluation);
            this.panel6.Controls.Add(this.sidcombobox);
            this.panel6.Controls.Add(this.guna2DateTimePicker6);
            this.panel6.Controls.Add(this.guna2HtmlLabel23);
            this.panel6.Controls.Add(this.guna2HtmlLabel25);
            this.panel6.Controls.Add(this.guna2HtmlLabel26);
            this.panel6.Controls.Add(this.guna2HtmlLabel27);
            this.panel6.Location = new System.Drawing.Point(2, 125);
            this.panel6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(253, 478);
            this.panel6.TabIndex = 1;
            // 
            // resultId
            // 
            this.resultId.AutoSize = true;
            this.resultId.Location = new System.Drawing.Point(94, 422);
            this.resultId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.resultId.Name = "resultId";
            this.resultId.Size = new System.Drawing.Size(20, 19);
            this.resultId.TabIndex = 19;
            this.resultId.Text = "N";
            // 
            // acidcombobox
            // 
            this.acidcombobox.BackColor = System.Drawing.Color.Transparent;
            this.acidcombobox.BorderRadius = 5;
            this.acidcombobox.BorderThickness = 3;
            this.acidcombobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.acidcombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.acidcombobox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.acidcombobox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.acidcombobox.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.acidcombobox.ForeColor = System.Drawing.Color.Black;
            this.acidcombobox.ItemHeight = 30;
            this.acidcombobox.Location = new System.Drawing.Point(49, 198);
            this.acidcombobox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.acidcombobox.Name = "acidcombobox";
            this.acidcombobox.Size = new System.Drawing.Size(188, 36);
            this.acidcombobox.TabIndex = 18;
            // 
            // RMidcombobox
            // 
            this.RMidcombobox.BackColor = System.Drawing.Color.Transparent;
            this.RMidcombobox.BorderRadius = 5;
            this.RMidcombobox.BorderThickness = 3;
            this.RMidcombobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.RMidcombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RMidcombobox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RMidcombobox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RMidcombobox.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.RMidcombobox.ForeColor = System.Drawing.Color.Black;
            this.RMidcombobox.ItemHeight = 30;
            this.RMidcombobox.Location = new System.Drawing.Point(49, 273);
            this.RMidcombobox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.RMidcombobox.Name = "RMidcombobox";
            this.RMidcombobox.Size = new System.Drawing.Size(188, 36);
            this.RMidcombobox.TabIndex = 17;
            // 
            // guna2HtmlLabel28
            // 
            this.guna2HtmlLabel28.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel28.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel28.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel28.Location = new System.Drawing.Point(8, 240);
            this.guna2HtmlLabel28.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel28.Name = "guna2HtmlLabel28";
            this.guna2HtmlLabel28.Size = new System.Drawing.Size(186, 27);
            this.guna2HtmlLabel28.TabIndex = 16;
            this.guna2HtmlLabel28.Text = "Rubric Measurement Id:";
            // 
            // dateEvaluation
            // 
            this.dateEvaluation.Animated = true;
            this.dateEvaluation.BorderColor = System.Drawing.SystemColors.GrayText;
            this.dateEvaluation.BorderRadius = 10;
            this.dateEvaluation.BorderThickness = 2;
            this.dateEvaluation.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dateEvaluation.DefaultText = "";
            this.dateEvaluation.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.dateEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.dateEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateEvaluation.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.dateEvaluation.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateEvaluation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dateEvaluation.ForeColor = System.Drawing.Color.Black;
            this.dateEvaluation.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.dateEvaluation.Location = new System.Drawing.Point(20, 344);
            this.dateEvaluation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dateEvaluation.Name = "dateEvaluation";
            this.dateEvaluation.PasswordChar = '\0';
            this.dateEvaluation.PlaceholderText = "yyyy-dd-mm";
            this.dateEvaluation.ReadOnly = true;
            this.dateEvaluation.SelectedText = "";
            this.dateEvaluation.Size = new System.Drawing.Size(200, 30);
            this.dateEvaluation.TabIndex = 15;
            // 
            // sidcombobox
            // 
            this.sidcombobox.BackColor = System.Drawing.Color.Transparent;
            this.sidcombobox.BorderRadius = 5;
            this.sidcombobox.BorderThickness = 3;
            this.sidcombobox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.sidcombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sidcombobox.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.sidcombobox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.sidcombobox.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sidcombobox.ForeColor = System.Drawing.Color.Black;
            this.sidcombobox.ItemHeight = 30;
            this.sidcombobox.Location = new System.Drawing.Point(49, 123);
            this.sidcombobox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.sidcombobox.Name = "sidcombobox";
            this.sidcombobox.Size = new System.Drawing.Size(188, 36);
            this.sidcombobox.TabIndex = 14;
            // 
            // guna2DateTimePicker6
            // 
            this.guna2DateTimePicker6.Checked = true;
            this.guna2DateTimePicker6.CustomFormat = "yyyy/dd/MM";
            this.guna2DateTimePicker6.FillColor = System.Drawing.Color.Silver;
            this.guna2DateTimePicker6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.guna2DateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.guna2DateTimePicker6.Location = new System.Drawing.Point(20, 381);
            this.guna2DateTimePicker6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2DateTimePicker6.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.guna2DateTimePicker6.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.guna2DateTimePicker6.Name = "guna2DateTimePicker6";
            this.guna2DateTimePicker6.Size = new System.Drawing.Size(200, 30);
            this.guna2DateTimePicker6.TabIndex = 13;
            this.guna2DateTimePicker6.Value = new System.DateTime(2024, 3, 5, 21, 20, 28, 0);
            this.guna2DateTimePicker6.ValueChanged += new System.EventHandler(this.guna2DateTimePicker6_ValueChanged);
            // 
            // guna2HtmlLabel23
            // 
            this.guna2HtmlLabel23.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel23.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel23.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel23.Location = new System.Drawing.Point(49, 55);
            this.guna2HtmlLabel23.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel23.Name = "guna2HtmlLabel23";
            this.guna2HtmlLabel23.Size = new System.Drawing.Size(156, 32);
            this.guna2HtmlLabel23.TabIndex = 6;
            this.guna2HtmlLabel23.Text = "Manage Result";
            // 
            // guna2HtmlLabel25
            // 
            this.guna2HtmlLabel25.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel25.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel25.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel25.Location = new System.Drawing.Point(8, 92);
            this.guna2HtmlLabel25.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel25.Name = "guna2HtmlLabel25";
            this.guna2HtmlLabel25.Size = new System.Drawing.Size(81, 23);
            this.guna2HtmlLabel25.TabIndex = 3;
            this.guna2HtmlLabel25.Text = "Student Id:";
            // 
            // guna2HtmlLabel26
            // 
            this.guna2HtmlLabel26.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel26.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel26.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel26.Location = new System.Drawing.Point(8, 165);
            this.guna2HtmlLabel26.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel26.Name = "guna2HtmlLabel26";
            this.guna2HtmlLabel26.Size = new System.Drawing.Size(228, 27);
            this.guna2HtmlLabel26.TabIndex = 2;
            this.guna2HtmlLabel26.Text = "Asseessment Component Id:";
            // 
            // guna2HtmlLabel27
            // 
            this.guna2HtmlLabel27.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel27.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel27.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel27.Location = new System.Drawing.Point(8, 315);
            this.guna2HtmlLabel27.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2HtmlLabel27.Name = "guna2HtmlLabel27";
            this.guna2HtmlLabel27.Size = new System.Drawing.Size(121, 23);
            this.guna2HtmlLabel27.TabIndex = 0;
            this.guna2HtmlLabel27.Text = "Evaluation Date:";
            // 
            // ReportTab
            // 
            this.ReportTab.BackColor = System.Drawing.Color.DarkCyan;
            this.ReportTab.Controls.Add(this.generateButton);
            this.ReportTab.Controls.Add(this.pictureBox15);
            this.ReportTab.Controls.Add(this.guna2HtmlLabel32);
            this.ReportTab.Controls.Add(this.pictureBox14);
            this.ReportTab.Controls.Add(this.tablename);
            this.ReportTab.Controls.Add(this.titlebox);
            this.ReportTab.Controls.Add(this.querybox);
            this.ReportTab.Controls.Add(this.guna2HtmlLabel31);
            this.ReportTab.Controls.Add(this.guna2HtmlLabel30);
            this.ReportTab.Controls.Add(this.guna2HtmlLabel29);
            this.ReportTab.Controls.Add(this.guna2GradientPanel7);
            this.ReportTab.Location = new System.Drawing.Point(5, 4);
            this.ReportTab.Name = "ReportTab";
            this.ReportTab.Padding = new System.Windows.Forms.Padding(3);
            this.ReportTab.Size = new System.Drawing.Size(892, 612);
            this.ReportTab.TabIndex = 13;
            this.ReportTab.Text = "tabPage1";
            // 
            // generateButton
            // 
            this.generateButton.BackColor = System.Drawing.Color.Transparent;
            this.generateButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.generateButton.BorderRadius = 6;
            this.generateButton.BorderThickness = 2;
            this.generateButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.generateButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.generateButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.generateButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.generateButton.FillColor = System.Drawing.Color.SlateGray;
            this.generateButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateButton.ForeColor = System.Drawing.Color.White;
            this.generateButton.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.generateButton.HoverState.FillColor = System.Drawing.Color.LightSlateGray;
            this.generateButton.HoverState.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateButton.Image = global::DB_Mid_Project.Properties.Resources.k27_removebg_preview;
            this.generateButton.Location = new System.Drawing.Point(279, 336);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(133, 44);
            this.generateButton.TabIndex = 18;
            this.generateButton.Text = "Generate ";
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox15.Image = global::DB_Mid_Project.Properties.Resources.k2_removebg_preview;
            this.pictureBox15.Location = new System.Drawing.Point(-5, 3);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(125, 100);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 17;
            this.pictureBox15.TabStop = false;
            // 
            // guna2HtmlLabel32
            // 
            this.guna2HtmlLabel32.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel32.Font = new System.Drawing.Font("Segoe UI Black", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel32.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel32.Location = new System.Drawing.Point(279, 19);
            this.guna2HtmlLabel32.Name = "guna2HtmlLabel32";
            this.guna2HtmlLabel32.Size = new System.Drawing.Size(172, 32);
            this.guna2HtmlLabel32.TabIndex = 15;
            this.guna2HtmlLabel32.Text = "Manage Reports";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pictureBox14.Image = global::DB_Mid_Project.Properties.Resources.k9i_removebg_preview;
            this.pictureBox14.Location = new System.Drawing.Point(794, -1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(99, 78);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 16;
            this.pictureBox14.TabStop = false;
            // 
            // tablename
            // 
            this.tablename.Animated = true;
            this.tablename.BorderColor = System.Drawing.SystemColors.GrayText;
            this.tablename.BorderRadius = 10;
            this.tablename.BorderThickness = 2;
            this.tablename.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tablename.DefaultText = "";
            this.tablename.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.tablename.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.tablename.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tablename.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.tablename.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tablename.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tablename.ForeColor = System.Drawing.Color.Black;
            this.tablename.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.tablename.Location = new System.Drawing.Point(257, 293);
            this.tablename.Name = "tablename";
            this.tablename.PasswordChar = '\0';
            this.tablename.PlaceholderText = "e.g.table1";
            this.tablename.SelectedText = "";
            this.tablename.Size = new System.Drawing.Size(200, 30);
            this.tablename.TabIndex = 14;
            // 
            // titlebox
            // 
            this.titlebox.Animated = true;
            this.titlebox.BorderColor = System.Drawing.SystemColors.GrayText;
            this.titlebox.BorderRadius = 10;
            this.titlebox.BorderThickness = 2;
            this.titlebox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.titlebox.DefaultText = "";
            this.titlebox.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.titlebox.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.titlebox.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.titlebox.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.titlebox.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.titlebox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.titlebox.ForeColor = System.Drawing.Color.Black;
            this.titlebox.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.titlebox.Location = new System.Drawing.Point(257, 233);
            this.titlebox.Name = "titlebox";
            this.titlebox.PasswordChar = '\0';
            this.titlebox.PlaceholderText = "e.g. max students";
            this.titlebox.SelectedText = "";
            this.titlebox.Size = new System.Drawing.Size(200, 30);
            this.titlebox.TabIndex = 13;
            // 
            // querybox
            // 
            this.querybox.Location = new System.Drawing.Point(257, 104);
            this.querybox.Name = "querybox";
            this.querybox.Size = new System.Drawing.Size(289, 79);
            this.querybox.TabIndex = 7;
            this.querybox.Text = "";
            // 
            // guna2HtmlLabel31
            // 
            this.guna2HtmlLabel31.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel31.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel31.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel31.Location = new System.Drawing.Point(155, 195);
            this.guna2HtmlLabel31.Name = "guna2HtmlLabel31";
            this.guna2HtmlLabel31.Size = new System.Drawing.Size(94, 23);
            this.guna2HtmlLabel31.TabIndex = 6;
            this.guna2HtmlLabel31.Text = "Report Title:";
            // 
            // guna2HtmlLabel30
            // 
            this.guna2HtmlLabel30.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel30.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel30.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel30.Location = new System.Drawing.Point(155, 266);
            this.guna2HtmlLabel30.Name = "guna2HtmlLabel30";
            this.guna2HtmlLabel30.Size = new System.Drawing.Size(92, 23);
            this.guna2HtmlLabel30.TabIndex = 5;
            this.guna2HtmlLabel30.Text = "Table Name:";
            // 
            // guna2HtmlLabel29
            // 
            this.guna2HtmlLabel29.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel29.Font = new System.Drawing.Font("Papyrus", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel29.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel29.Location = new System.Drawing.Point(159, 71);
            this.guna2HtmlLabel29.Name = "guna2HtmlLabel29";
            this.guna2HtmlLabel29.Size = new System.Drawing.Size(95, 23);
            this.guna2HtmlLabel29.TabIndex = 4;
            this.guna2HtmlLabel29.Text = "Write Query:";
            // 
            // guna2GradientPanel7
            // 
            this.guna2GradientPanel7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.guna2GradientPanel7.AutoSize = true;
            this.guna2GradientPanel7.BorderColor = System.Drawing.Color.Teal;
            this.guna2GradientPanel7.BorderThickness = 4;
            this.guna2GradientPanel7.Controls.Add(this.report5buttton);
            this.guna2GradientPanel7.Controls.Add(this.report4button);
            this.guna2GradientPanel7.Controls.Add(this.report3button);
            this.guna2GradientPanel7.Controls.Add(this.report2button);
            this.guna2GradientPanel7.Controls.Add(this.report1button);
            this.guna2GradientPanel7.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel7.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel7.Location = new System.Drawing.Point(3, 386);
            this.guna2GradientPanel7.Name = "guna2GradientPanel7";
            this.guna2GradientPanel7.Size = new System.Drawing.Size(888, 223);
            this.guna2GradientPanel7.TabIndex = 2;
            // 
            // report5buttton
            // 
            this.report5buttton.BackColor = System.Drawing.Color.Transparent;
            this.report5buttton.BorderColor = System.Drawing.Color.DarkOliveGreen;
            this.report5buttton.BorderRadius = 6;
            this.report5buttton.BorderThickness = 2;
            this.report5buttton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.report5buttton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.report5buttton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.report5buttton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.report5buttton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.report5buttton.FillColor = System.Drawing.Color.SeaGreen;
            this.report5buttton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report5buttton.ForeColor = System.Drawing.Color.White;
            this.report5buttton.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.report5buttton.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.report5buttton.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report5buttton.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.report5buttton.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.report5buttton.ImageSize = new System.Drawing.Size(40, 40);
            this.report5buttton.Location = new System.Drawing.Point(0, 179);
            this.report5buttton.Name = "report5buttton";
            this.report5buttton.Size = new System.Drawing.Size(888, 44);
            this.report5buttton.TabIndex = 4;
            this.report5buttton.Text = "Students Who have Left ";
            this.report5buttton.Click += new System.EventHandler(this.report5buttton_Click);
            // 
            // report4button
            // 
            this.report4button.BackColor = System.Drawing.Color.Transparent;
            this.report4button.BorderColor = System.Drawing.Color.DarkOliveGreen;
            this.report4button.BorderRadius = 6;
            this.report4button.BorderThickness = 2;
            this.report4button.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.report4button.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.report4button.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.report4button.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.report4button.Dock = System.Windows.Forms.DockStyle.Top;
            this.report4button.FillColor = System.Drawing.Color.SeaGreen;
            this.report4button.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report4button.ForeColor = System.Drawing.Color.White;
            this.report4button.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.report4button.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.report4button.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report4button.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.report4button.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.report4button.ImageSize = new System.Drawing.Size(40, 40);
            this.report4button.Location = new System.Drawing.Point(0, 132);
            this.report4button.Name = "report4button";
            this.report4button.Size = new System.Drawing.Size(888, 44);
            this.report4button.TabIndex = 3;
            this.report4button.Text = "Result With Evaluation Date";
            this.report4button.Click += new System.EventHandler(this.report4button_Click);
            // 
            // report3button
            // 
            this.report3button.BackColor = System.Drawing.Color.Transparent;
            this.report3button.BorderColor = System.Drawing.Color.Teal;
            this.report3button.BorderRadius = 6;
            this.report3button.BorderThickness = 2;
            this.report3button.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.report3button.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.report3button.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.report3button.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.report3button.Dock = System.Windows.Forms.DockStyle.Top;
            this.report3button.FillColor = System.Drawing.Color.SeaGreen;
            this.report3button.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report3button.ForeColor = System.Drawing.Color.White;
            this.report3button.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.report3button.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.report3button.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report3button.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.report3button.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.report3button.ImageSize = new System.Drawing.Size(40, 40);
            this.report3button.Location = new System.Drawing.Point(0, 88);
            this.report3button.Name = "report3button";
            this.report3button.Size = new System.Drawing.Size(888, 44);
            this.report3button.TabIndex = 2;
            this.report3button.Text = "Date Wise Attendance Report of Students";
            this.report3button.Click += new System.EventHandler(this.report3button_Click);
            // 
            // report2button
            // 
            this.report2button.BackColor = System.Drawing.Color.Transparent;
            this.report2button.BorderColor = System.Drawing.Color.Teal;
            this.report2button.BorderRadius = 6;
            this.report2button.BorderThickness = 2;
            this.report2button.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.report2button.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.report2button.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.report2button.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.report2button.Dock = System.Windows.Forms.DockStyle.Top;
            this.report2button.FillColor = System.Drawing.Color.SeaGreen;
            this.report2button.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report2button.ForeColor = System.Drawing.Color.White;
            this.report2button.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.report2button.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.report2button.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report2button.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.report2button.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.report2button.ImageSize = new System.Drawing.Size(40, 40);
            this.report2button.Location = new System.Drawing.Point(0, 44);
            this.report2button.Name = "report2button";
            this.report2button.Size = new System.Drawing.Size(888, 44);
            this.report2button.TabIndex = 1;
            this.report2button.Text = "Assessment Wise Class Result";
            this.report2button.Click += new System.EventHandler(this.guna2Button37_Click);
            // 
            // report1button
            // 
            this.report1button.BackColor = System.Drawing.Color.Transparent;
            this.report1button.BorderColor = System.Drawing.Color.Teal;
            this.report1button.BorderRadius = 6;
            this.report1button.BorderThickness = 2;
            this.report1button.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.report1button.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.report1button.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.report1button.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.report1button.Dock = System.Windows.Forms.DockStyle.Top;
            this.report1button.FillColor = System.Drawing.Color.SeaGreen;
            this.report1button.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report1button.ForeColor = System.Drawing.Color.White;
            this.report1button.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.report1button.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.report1button.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.report1button.Image = global::DB_Mid_Project.Properties.Resources.k20i_removebg_preview__1_;
            this.report1button.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.report1button.ImageSize = new System.Drawing.Size(40, 40);
            this.report1button.Location = new System.Drawing.Point(0, 0);
            this.report1button.Name = "report1button";
            this.report1button.Size = new System.Drawing.Size(888, 44);
            this.report1button.TabIndex = 0;
            this.report1button.Text = "CLO Wise Class Report";
            this.report1button.Click += new System.EventHandler(this.report1button_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.pictureBox3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.guna2GradientPanel2, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(901, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.51852F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 81.48148F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(286, 620);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.LightCyan;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Image = global::DB_Mid_Project.Properties.Resources.k18i_removebg_preview;
            this.pictureBox3.Location = new System.Drawing.Point(2, 3);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(282, 108);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // guna2GradientPanel2
            // 
            this.guna2GradientPanel2.BorderColor = System.Drawing.Color.Red;
            this.guna2GradientPanel2.Controls.Add(this.guna2Button25);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button24);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button14);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button13);
            this.guna2GradientPanel2.Controls.Add(this.guna2TextBox8);
            this.guna2GradientPanel2.Controls.Add(this.guna2PictureBox1);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button12);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button11);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button10);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button9);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button8);
            this.guna2GradientPanel2.Controls.Add(this.guna2Button7);
            this.guna2GradientPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2GradientPanel2.FillColor = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel2.FillColor2 = System.Drawing.Color.DarkCyan;
            this.guna2GradientPanel2.Location = new System.Drawing.Point(2, 117);
            this.guna2GradientPanel2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2GradientPanel2.Name = "guna2GradientPanel2";
            this.guna2GradientPanel2.Size = new System.Drawing.Size(282, 500);
            this.guna2GradientPanel2.TabIndex = 1;
            // 
            // guna2Button25
            // 
            this.guna2Button25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button25.BackColor = System.Drawing.Color.Navy;
            this.guna2Button25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button25.BorderThickness = 2;
            this.guna2Button25.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button25.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button25.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button25.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button25.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button25.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button25.ForeColor = System.Drawing.Color.White;
            this.guna2Button25.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button25.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button25.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button25.Image = global::DB_Mid_Project.Properties.Resources.k11i_removebg_preview;
            this.guna2Button25.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button25.Location = new System.Drawing.Point(8, 406);
            this.guna2Button25.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button25.Name = "guna2Button25";
            this.guna2Button25.Size = new System.Drawing.Size(265, 32);
            this.guna2Button25.TabIndex = 14;
            this.guna2Button25.Text = "Manage Report ";
            this.guna2Button25.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button25.Click += new System.EventHandler(this.guna2Button25_Click);
            // 
            // guna2Button24
            // 
            this.guna2Button24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button24.BackColor = System.Drawing.Color.Navy;
            this.guna2Button24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button24.BorderThickness = 2;
            this.guna2Button24.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button24.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button24.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button24.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button24.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button24.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button24.ForeColor = System.Drawing.Color.White;
            this.guna2Button24.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button24.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button24.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button24.Image = global::DB_Mid_Project.Properties.Resources.k9i_removebg_preview;
            this.guna2Button24.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button24.Location = new System.Drawing.Point(7, 225);
            this.guna2Button24.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button24.Name = "guna2Button24";
            this.guna2Button24.Size = new System.Drawing.Size(269, 32);
            this.guna2Button24.TabIndex = 13;
            this.guna2Button24.Text = "Manage Assessment Component";
            this.guna2Button24.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button24.Click += new System.EventHandler(this.guna2Button24_Click);
            // 
            // guna2Button14
            // 
            this.guna2Button14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button14.BackColor = System.Drawing.Color.Navy;
            this.guna2Button14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button14.BorderThickness = 2;
            this.guna2Button14.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button14.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button14.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button14.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button14.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button14.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button14.ForeColor = System.Drawing.Color.White;
            this.guna2Button14.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button14.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button14.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button14.Image = global::DB_Mid_Project.Properties.Resources.k16i_removebg_preview;
            this.guna2Button14.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button14.ImageSize = new System.Drawing.Size(20, 30);
            this.guna2Button14.Location = new System.Drawing.Point(6, 298);
            this.guna2Button14.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button14.Name = "guna2Button14";
            this.guna2Button14.Size = new System.Drawing.Size(269, 32);
            this.guna2Button14.TabIndex = 12;
            this.guna2Button14.Text = "Manage Rubrics ";
            this.guna2Button14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button14.Click += new System.EventHandler(this.guna2Button14_Click);
            // 
            // guna2Button13
            // 
            this.guna2Button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button13.BorderThickness = 2;
            this.guna2Button13.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button13.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button13.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button13.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button13.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button13.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button13.ForeColor = System.Drawing.Color.White;
            this.guna2Button13.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button13.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button13.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button13.Image = global::DB_Mid_Project.Properties.Resources.k3i_removebg_preview;
            this.guna2Button13.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button13.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button13.Location = new System.Drawing.Point(4, 120);
            this.guna2Button13.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button13.Name = "guna2Button13";
            this.guna2Button13.PressedColor = System.Drawing.SystemColors.ControlDark;
            this.guna2Button13.Size = new System.Drawing.Size(269, 30);
            this.guna2Button13.TabIndex = 11;
            this.guna2Button13.Text = "Manage Students";
            this.guna2Button13.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button13.Click += new System.EventHandler(this.guna2Button13_Click);
            // 
            // guna2TextBox8
            // 
            this.guna2TextBox8.BorderColor = System.Drawing.Color.Gray;
            this.guna2TextBox8.BorderThickness = 0;
            this.guna2TextBox8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox8.DefaultText = "DASHBOARD";
            this.guna2TextBox8.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox8.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2TextBox8.FillColor = System.Drawing.Color.LightCyan;
            this.guna2TextBox8.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox8.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.guna2TextBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2TextBox8.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox8.Location = new System.Drawing.Point(0, 82);
            this.guna2TextBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2TextBox8.Name = "guna2TextBox8";
            this.guna2TextBox8.PasswordChar = '\0';
            this.guna2TextBox8.PlaceholderText = "";
            this.guna2TextBox8.ReadOnly = true;
            this.guna2TextBox8.SelectedText = "";
            this.guna2TextBox8.Size = new System.Drawing.Size(282, 27);
            this.guna2TextBox8.TabIndex = 10;
            this.guna2TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.BackColor = System.Drawing.Color.LightCyan;
            this.guna2PictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2PictureBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.guna2PictureBox1.Image = global::DB_Mid_Project.Properties.Resources.k12i_removebg_preview;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.guna2PictureBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(282, 82);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 9;
            this.guna2PictureBox1.TabStop = false;
            // 
            // guna2Button12
            // 
            this.guna2Button12.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button12.BorderRadius = 6;
            this.guna2Button12.BorderThickness = 2;
            this.guna2Button12.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button12.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button12.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button12.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2Button12.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button12.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button12.ForeColor = System.Drawing.Color.White;
            this.guna2Button12.HoverState.BorderColor = System.Drawing.Color.Olive;
            this.guna2Button12.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2Button12.HoverState.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button12.Image = global::DB_Mid_Project.Properties.Resources._28_removebg_preview;
            this.guna2Button12.ImageSize = new System.Drawing.Size(35, 35);
            this.guna2Button12.Location = new System.Drawing.Point(0, 457);
            this.guna2Button12.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button12.Name = "guna2Button12";
            this.guna2Button12.Size = new System.Drawing.Size(282, 43);
            this.guna2Button12.TabIndex = 4;
            this.guna2Button12.Text = "Exit";
            this.guna2Button12.Click += new System.EventHandler(this.guna2Button12_Click);
            // 
            // guna2Button11
            // 
            this.guna2Button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button11.BackColor = System.Drawing.Color.Navy;
            this.guna2Button11.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button11.BorderThickness = 2;
            this.guna2Button11.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button11.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button11.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button11.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button11.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button11.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button11.ForeColor = System.Drawing.Color.White;
            this.guna2Button11.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button11.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button11.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button11.Image = global::DB_Mid_Project.Properties.Resources.k10i_removebg_preview;
            this.guna2Button11.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button11.Location = new System.Drawing.Point(10, 370);
            this.guna2Button11.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button11.Name = "guna2Button11";
            this.guna2Button11.Size = new System.Drawing.Size(265, 32);
            this.guna2Button11.TabIndex = 8;
            this.guna2Button11.Text = "Manage Result     ";
            this.guna2Button11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button11.Click += new System.EventHandler(this.guna2Button11_Click);
            // 
            // guna2Button10
            // 
            this.guna2Button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button10.BackColor = System.Drawing.Color.Navy;
            this.guna2Button10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button10.BorderThickness = 2;
            this.guna2Button10.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button10.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button10.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button10.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button10.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button10.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button10.ForeColor = System.Drawing.Color.White;
            this.guna2Button10.HoverState.BorderColor = System.Drawing.Color.Gray;
            this.guna2Button10.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button10.HoverState.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button10.Image = global::DB_Mid_Project.Properties.Resources.k27_removebg_preview;
            this.guna2Button10.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button10.Location = new System.Drawing.Point(7, 335);
            this.guna2Button10.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button10.Name = "guna2Button10";
            this.guna2Button10.Size = new System.Drawing.Size(269, 32);
            this.guna2Button10.TabIndex = 7;
            this.guna2Button10.Text = "Manage Rubric Level ";
            this.guna2Button10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button10.Click += new System.EventHandler(this.guna2Button10_Click);
            // 
            // guna2Button9
            // 
            this.guna2Button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button9.BackColor = System.Drawing.Color.Navy;
            this.guna2Button9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button9.BorderThickness = 2;
            this.guna2Button9.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button9.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button9.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button9.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button9.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button9.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button9.ForeColor = System.Drawing.Color.White;
            this.guna2Button9.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button9.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button9.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button9.Image = global::DB_Mid_Project.Properties.Resources.k11i_removebg_preview;
            this.guna2Button9.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button9.Location = new System.Drawing.Point(7, 262);
            this.guna2Button9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button9.Name = "guna2Button9";
            this.guna2Button9.Size = new System.Drawing.Size(269, 32);
            this.guna2Button9.TabIndex = 6;
            this.guna2Button9.Text = "Manage CLOs ";
            this.guna2Button9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button9.Click += new System.EventHandler(this.guna2Button9_Click);
            // 
            // guna2Button8
            // 
            this.guna2Button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button8.BackColor = System.Drawing.Color.Navy;
            this.guna2Button8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button8.BorderThickness = 2;
            this.guna2Button8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button8.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button8.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button8.ForeColor = System.Drawing.Color.White;
            this.guna2Button8.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button8.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button8.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button8.Image = global::DB_Mid_Project.Properties.Resources.k8i_removebg_preview;
            this.guna2Button8.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button8.Location = new System.Drawing.Point(4, 188);
            this.guna2Button8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button8.Name = "guna2Button8";
            this.guna2Button8.Size = new System.Drawing.Size(269, 32);
            this.guna2Button8.TabIndex = 5;
            this.guna2Button8.Text = "Manage Assessment ";
            this.guna2Button8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button8.Click += new System.EventHandler(this.guna2Button8_Click);
            // 
            // guna2Button7
            // 
            this.guna2Button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button7.BackColor = System.Drawing.Color.Navy;
            this.guna2Button7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.guna2Button7.BorderThickness = 2;
            this.guna2Button7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button7.FillColor = System.Drawing.Color.SeaGreen;
            this.guna2Button7.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button7.ForeColor = System.Drawing.Color.White;
            this.guna2Button7.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.guna2Button7.HoverState.FillColor = System.Drawing.Color.DarkOliveGreen;
            this.guna2Button7.HoverState.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button7.Image = global::DB_Mid_Project.Properties.Resources.k7i_removebg_preview;
            this.guna2Button7.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button7.Location = new System.Drawing.Point(2, 154);
            this.guna2Button7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.guna2Button7.Name = "guna2Button7";
            this.guna2Button7.Size = new System.Drawing.Size(269, 30);
            this.guna2Button7.TabIndex = 4;
            this.guna2Button7.Text = "Manage Attendance    ";
            this.guna2Button7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button7.Click += new System.EventHandler(this.guna2Button7_Click);
            // 
            // CLO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkCyan;
            this.ClientSize = new System.Drawing.Size(1187, 620);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.guna2TabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "CLO";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CLO";
            this.guna2TabControl1.ResumeLayout(false);
            this.RubricTab.ResumeLayout(false);
            this.guna2GradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.RubricLevelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.guna2GradientPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.AssessmentTab.ResumeLayout(false);
            this.AssessmentTab.PerformLayout();
            this.guna2GradientPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.CloTab.ResumeLayout(false);
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.AssessmentComponentTab.ResumeLayout(false);
            this.guna2GradientPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResultTab.ResumeLayout(false);
            this.ResultTab.PerformLayout();
            this.guna2GradientPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ReportTab.ResumeLayout(false);
            this.ReportTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.guna2GradientPanel7.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.guna2GradientPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2TabControl guna2TabControl1;
        private System.Windows.Forms.TabPage RubricTab;
        private System.Windows.Forms.TabPage RubricLevelTab;
        private System.Windows.Forms.TabPage AssessmentTab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel2;
        private Guna.UI2.WinForms.Guna2Button guna2Button13;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox8;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2Button guna2Button12;
        private Guna.UI2.WinForms.Guna2Button guna2Button11;
        private Guna.UI2.WinForms.Guna2Button guna2Button10;
        private Guna.UI2.WinForms.Guna2Button guna2Button9;
        private Guna.UI2.WinForms.Guna2Button guna2Button8;
        private Guna.UI2.WinForms.Guna2Button guna2Button7;
        private System.Windows.Forms.TabPage CloTab;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox3;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel7;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel6;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel4;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2Button guna2Button16;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker2;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker1;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox5;
        private System.Windows.Forms.PictureBox pictureBox5;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel5;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel8;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox1;
        private Guna.UI2.WinForms.Guna2Button guna2Button14;
        private System.Windows.Forms.DataGridView dataGridView2;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel1;
        private Guna.UI2.WinForms.Guna2Button guna2Button15;
        private Guna.UI2.WinForms.Guna2Button guna2Button17;
        private Guna.UI2.WinForms.Guna2Button guna2Button18;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel3;
        private Guna.UI2.WinForms.Guna2Button guna2Button19;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel9;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel3;
        private Guna.UI2.WinForms.Guna2Button guna2Button21;
        private Guna.UI2.WinForms.Guna2Button guna2Button22;
        private System.Windows.Forms.DataGridView dataGridView3;
        private Guna.UI2.WinForms.Guna2ComboBox rubricIDBox;
        private Guna.UI2.WinForms.Guna2Button guna2Button25;
        private Guna.UI2.WinForms.Guna2Button guna2Button24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel4;
        private Guna.UI2.WinForms.Guna2TextBox dateassesssment;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker3;
        private Guna.UI2.WinForms.Guna2TextBox title;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel11;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel12;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel13;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel14;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.DataGridView dataGridView4;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel4;
        private Guna.UI2.WinForms.Guna2Button guna2Button20;
        private Guna.UI2.WinForms.Guna2Button guna2Button23;
        private Guna.UI2.WinForms.Guna2Button guna2Button6;
        private Guna.UI2.WinForms.Guna2TextBox tweight;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel15;
        private Guna.UI2.WinForms.Guna2TextBox tmarks;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2Button guna2Button26;
        private System.Windows.Forms.Label id;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel16;
        private System.Windows.Forms.TabPage AssessmentComponentTab;
        private System.Windows.Forms.TabPage ResultTab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel5;
        private Guna.UI2.WinForms.Guna2TextBox dateUpdated;
        private Guna.UI2.WinForms.Guna2ComboBox RidcomboBox;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker4;
        private Guna.UI2.WinForms.Guna2TextBox componentname;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel17;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel18;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel19;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel20;
        private System.Windows.Forms.PictureBox pictureBox11;
        private Guna.UI2.WinForms.Guna2TextBox DateCreated;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker5;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel22;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel21;
        private Guna.UI2.WinForms.Guna2TextBox compoMarks;
        private Guna.UI2.WinForms.Guna2ComboBox assessmentidComboBox;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel24;
        private System.Windows.Forms.DataGridView dataGridView5;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel5;
        private Guna.UI2.WinForms.Guna2Button guna2Button27;
        private Guna.UI2.WinForms.Guna2Button guna2Button28;
        private Guna.UI2.WinForms.Guna2Button guna2Button29;
        private Guna.UI2.WinForms.Guna2Button guna2Button30;
        private System.Windows.Forms.Label ACId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel6;
        private Guna.UI2.WinForms.Guna2TextBox dateEvaluation;
        private Guna.UI2.WinForms.Guna2ComboBox sidcombobox;
        private Guna.UI2.WinForms.Guna2DateTimePicker guna2DateTimePicker6;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel23;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel25;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel26;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel27;
        private Guna.UI2.WinForms.Guna2ComboBox acidcombobox;
        private Guna.UI2.WinForms.Guna2ComboBox RMidcombobox;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel28;
        private System.Windows.Forms.DataGridView dataGridView6;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel6;
        private Guna.UI2.WinForms.Guna2Button guna2Button32;
        private Guna.UI2.WinForms.Guna2Button guna2Button33;
        private Guna.UI2.WinForms.Guna2Button guna2Button31;
        private Guna.UI2.WinForms.Guna2Button guna2Button34;
        private System.Windows.Forms.Label resultId;
        private System.Windows.Forms.TabPage ReportTab;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel7;
        private Guna.UI2.WinForms.Guna2Button report4button;
        private Guna.UI2.WinForms.Guna2Button report3button;
        private Guna.UI2.WinForms.Guna2Button report2button;
        private Guna.UI2.WinForms.Guna2Button report1button;
        private Guna.UI2.WinForms.Guna2Button report5buttton;
        private System.Windows.Forms.RichTextBox querybox;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel31;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel30;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel29;
        private Guna.UI2.WinForms.Guna2TextBox tablename;
        private Guna.UI2.WinForms.Guna2TextBox titlebox;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel32;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private Guna.UI2.WinForms.Guna2Button generateButton;
    }
}