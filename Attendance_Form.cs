﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Mid_Project
{
    
    public partial class Attendance_Form : Form
    {
        private Guna.UI2.WinForms.Guna2TabControl cloTabControl;
        private CLO cloform;
        public Attendance_Form( Guna.UI2.WinForms.Guna2TabControl TabControl)
        {
            InitializeComponent();
            cloform = new CLO();
            cloTabControl = TabControl;
            
        }

        private void guna2Button13_Click(object sender, EventArgs e)
        {
            this.Hide();
            Studentmanage sm = new Studentmanage(cloTabControl);
            sm.Show();
        }

        private void guna2Button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            if(guna2TextBox1.Text=="")
            {
                MessageBox.Show("Select A date of which you Want to See Attendance");
            }
            else
            {
                guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
                guna2DateTimePicker1.CustomFormat = "yyyy/dd/MM";
                guna2TextBox1.Text = guna2DateTimePicker1.Value.ToShortDateString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select S.RegistrationNumber As RegistrationNumber, C.AttendanceDate As Date ,L.Name As Status from StudentAttendance A join ClassAttendance C on A.AttendanceId=C.Id join Student S on  A.StudentId=S.Id join Lookup L on A.AttendanceStatus=L.LookupId where C.AttendanceDate= @date and S.Status=5 Order By S.RegistrationNumber,C.AttendanceDate", con);
                cmd.Parameters.AddWithValue("@date", guna2TextBox1.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
        }
        private void update_table()
        {
            guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker1.CustomFormat = "yyyy/dd/MM";
            guna2TextBox1.Text = guna2DateTimePicker1.Value.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select S.RegistrationNumber As RegistrationNumber, C.AttendanceDate As Date ,L.Name As Status from StudentAttendance A join ClassAttendance C on A.AttendanceId=C.Id join Student S on  A.StudentId=S.Id join Lookup L on A.AttendanceStatus=L.LookupId where C.AttendanceDate= @date and S.Status=5 Order By S.RegistrationNumber,C.AttendanceDate", con);
            cmd.Parameters.AddWithValue("@date", guna2TextBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

       
        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            guna2TextBox1.Clear();
            guna2TextBox5.Clear();
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                guna2TextBox5.Text = selectedRow.Cells["RegistrationNumber"].Value.ToString();
                guna2TextBox1.Text = selectedRow.Cells["Date"].Value.ToString();
                guna2TextBox5.ReadOnly=true;

            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
           
            string updateQuery = "UPDATE StudentAttendance SET AttendanceStatus=@Status WHERE StudentId=(Select Id from student where RegistrationNumber = @RegistrationNumber)";
            SqlCommand cmd = new SqlCommand(updateQuery, con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", guna2TextBox5.Text);
            cmd.Parameters.AddWithValue("@Status", guna2ComboBox1.SelectedIndex + 1);
            cmd.ExecuteNonQuery();
            update_table();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Saved Successfully..");
        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 2;

        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 3;
        }

        private void guna2DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker1.CustomFormat = "yyyy/dd/MM";
            guna2TextBox1.Text = guna2DateTimePicker1.Text;
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            string date;
            guna2DateTimePicker1.Format = DateTimePickerFormat.Custom;
            guna2DateTimePicker1.CustomFormat = "yyyy/dd/MM";
            guna2TextBox1.Text = guna2DateTimePicker1.Value.ToShortDateString();
            date = guna2TextBox1.Text;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("insert into ClassAttendance (AttendanceDate) select @date where not exists(select attendancedate from ClassAttendance where AttendanceDate=@date) ", con);
            cmd.Parameters.AddWithValue("@date", guna2TextBox1.Text);
            int row = cmd.ExecuteNonQuery();
            if (row == 0)
            {
                MessageBox.Show("Attendance Already Exists...");
            }
            else
            {
                MessageBox.Show("Attendance Added Successfully");

                var cone = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("INSERT INTO StudentAttendance (AttendanceId, StudentId, AttendanceStatus) SELECT (SELECT TOP 1 id AS AttendanceId FROM ClassAttendance WHERE AttendanceDate = @date) AS AttendanceId, Id AS StudentId, 1 AS AttendanceStatus FROM Student WHERE  Student.Status = 5", cone);
                cmd1.Parameters.AddWithValue("@date", date);
                cmd1.ExecuteNonQuery();

            }
        }
        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 4;
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 0;
        }

        private void guna2Button10_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 1;
        }

        private void guna2Button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 5;
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            cloform.Show();
            cloTabControl.SelectedIndex = 6;
        }
    }
}

